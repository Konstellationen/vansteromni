# Mediakollen

Mediakollen is a curated list of Swedish left news articles / pod episodes / blog entries.

It is a mix of being a web based RSS reader, a news portal and a link aggregator with inspiration
from [Hacker News](https://news.ycombinator.com/) and other sites.

Entries from those will appear in a list which a curator manually can process and "favourite" those entries that should appear in the curated feed on the site.

## Local development

This is a django site. Python is the programming language.

How to set up and run using Linux or MacOS with Homebrew:

```
python3 -m venv venv
source venv/bin/activate
python -m pip install -r requirements.txt
cp env.example .env
cd src
```

Then, either create a database from scratch using the dbshell command (can also be done using the web UI)

```
python manage.py migrate
# when prompted set e.g. username to admin, email to admin@localhost and password to adminadmin
python manage.py createsuperuser
# Then we have to set up some categories
python manage.py shell
# Then enter the following:
from feeds.models import FeedCategory
c1 = FeedCategory(name="News", description="Tidningar och nyheter", slug="nyheter")
c1.save()
c2 = FeedCategory(name="Blog", description="Bloggar", slug="bloggar")
c2.save()
c3 = FeedCategory(name="Podcast", description="Poddar", slug="poddar")
c3.save()
c4 = FeedCategory(name="Event", description="Event", slug="event")
c4.save()
# Now create a RSS item feed map so we know how to map RSS xml text to our database models
from feeds.models import ItemFeedMap
feedmap = ItemFeedMap(item_feed_map_name="Generic RSS", items="channel/item", title="title", link="link", item_id="guid", published="pubDate", updated="doesnotexist", content="content", summary="description", image_url="media/media")
feedmap.save()
# Now create a feed for a newspaper, a blog, a podcast and an event site
from feeds.models import Feed
newsfeed = Feed(name="Arbetaren", url="https://www.arbetaren.se/feed/", description="Syndikalistisk tidning sedan 1922", item_feed_map=feedmap, category=c1, political_affiliation="Syndikalistisk", publisher="SAC Syndikalisterna", website_url="https://www.arbetaren.se", mastodon_account_url="https://mastodon.nu/@tidningen_arbetaren", slug="arbetaren")
newsfeed.save()
blogfeed = Feed(name="Grow Sverige", url="https://growsverige.se/feed/", description="Miljöblogg", item_feed_map=feedmap, category=c2, political_affiliation="", publisher="", website_url="https://growsverige.se", mastodon_account_url="", slug="grow-sverige")
blogfeed.save()
podcastfeed = Feed(name="Apans anatomi", url="https://feeds.soundcloud.com/users/soundcloud:users:449604585/sounds.rss", description="Podd av Mathias Wååg", item_feed_map=feedmap, category=c3, political_affiliation="", publisher="", website_url="https://tidningenbrand.se/podd/apans-anatomi", mastodon_account_url="", slug="apans-anatomi")
podcastfeed.save()
eventfeed = Feed(name="Gnistor", url="https://www.gnistor.se/feed/index.xml", description="Gnistor kalendarium", item_feed_map=feedmap, category=c4, political_affiliation="", publisher="", website_url="https://www.gnistor.se", mastodon_account_url="", slug="gnistor")
eventfeed.save()
exit()
```

OR use an existing database:

```
cp /path/to/datadump.sqlite3 db.sqlite3
```

And start the builtin webserver

```
python manage.py runserver
```

Now visit http://localhost:8000. If you have used a database dump you should see a populated index page. If you have created a databse from scratch, you need to scrape feeds and also choose some items to be shown as curated on the index page.

## Running commands

There are a couple of commands that can be run from the commandline:

```
python manage.py list_feeds
# Scrapes the feed with id 1
python manage.py scrape_feed 1
# Scrape all feeeds
python manage.py scrape_feed all
```

## Using docker compose on live

We are using docker compose to deploy live.

```
# Either "docker-compose" or "docker compose" depending on your system
docker compose build
docker compose up -d
# The first time we need to create a super user. Enter username, email, password
docker compose exec web sh -c "python manage.py createsuperuser"
```

And then use commands for scraping feeds:

```
docker compose exec web sh -c "python manage.py list_feeds"
docker compose exec web sh -c "python manage.py scrape_feed 1"
```


## Using docker compose locally

One can also use docker compose locally when developing. But then we need a different docker-compose yaml file.

```
# Either "docker-compose" or "docker compose" depending on your system
# We need to add docker-compose.dev.yml to fake the external nginx_network
docker compose -f docker-compose.yml -f docker-compose.dev.yml build
docker compose -f docker-compose.yml -f docker-compose.dev.yml up -d
```

Surf to http://localhost

## For developers

To get a hang of Django, follow [this tutorial](https://docs.djangoproject.com/en/4.2/intro/tutorial01/).

We have the following "django apps":

* entries - all posts that will be shown to the users
* feeds - all feeds for newspapers, blogs, podcasts, etc

## Testing

Just do:

```
cd src
# All tests
python manage.py test
# All tests for feeds
python manage.py test feeds
# All tests for the feeds test class FunctionsTests
python manage.py test feeds.tests.FunctionsTests
# A specific test case for feeds
python manage.py test feeds.tests.FunctionsTests.test_feed_string_to_items_arbetaren_sample
```

## Deploy

### Backup

Before deploying any changes, create a backup of the current sqlite3 database:

In the root directory:

> make backup

Example output:
> ./scripts/backup.sh
> Backup klar: ../backups/mediakollen/manual/db-2023-11-02--14-31.sqlite3

Verify that the file is there:

> ls --lah ../backups/mediakollen/manual/

Proceed with deploy.

## CSS

## Update static cascading stylesheet

To update the cascading stylesheet for the project Tailwind has to read all src-files to know what classes to package into the static file. To do this either install Tailwind [as a standalone](https://tailwindcss.com/blog/standalone-cli) or [with Node.js](https://tailwindcss.com/docs/installation). Then run:

```
npx tailwindcss -i ./src/style.css -o ./src/static/style.css --watch
```

When you have changed the `src/static/style.css` file, make sure you bust the cache.

You can get the first 8 chars of the hash of the file:

```
$ sha256sum src/static/style.css|cut -c 1-8
9ed54656
```

Then you can edit `src/feeds/templates/base.html` and change the v parameter:

```
    <link rel='stylesheet' href='{% static 'style.css' %}?v=9ed54656' media='all' />
```


Without doing this, users will have a cached style.css file which they won't has been changed.

## Other resources

- https://heroicons.com
