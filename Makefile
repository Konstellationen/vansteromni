.PHONY: backup runserver install init-venv

backup:
	./scripts/backup.sh

start:
	python src/manage.py runserver

install:
	python -m pip install -r requirements.txt

init:
	python -m venv venv
	source .venv/bin/activate
