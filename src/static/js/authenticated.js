document.addEventListener('DOMContentLoaded', (e) => {
  // Like
  document.querySelectorAll('button[data-button-action="like"]').forEach((item) => {
      item.addEventListener('click', (e) => {
          const button = e.target.closest('button');
          const itemId = button.dataset.item_id;
          const xmlhtt = new XMLHttpRequest();
          const url = `/api/like/${itemId}/`;
          xmlhtt.open("POST", url, true);
          xmlhtt.setRequestHeader("Content-Type", "application/json");
          xmlhtt.send(null);
          xmlhtt.onreadystatechange = () => {
              if (xmlhtt.readyState == 4 && xmlhtt.status == 200) {
                  try {
                      const jsonData = JSON.parse(xmlhtt.responseText);
                      if (jsonData.success) {
                          item.setAttribute("disabled", "disabled");
                          item.innerHTML = "Gillat och klart";
                      } else {
                          console.error("Error occured with json data");
                          console.error(jsonData);
                      }
                  } catch (error) {
                      console.error("Error occured");
                      console.error(error);
                  }
              }
          };

      });
  });
  // Unlike
  document.querySelectorAll('button[data-button-action="unlike"]').forEach((item) => {
      item.addEventListener('click', (e) => {
          const button = e.target.closest('button');
          const itemId = button.dataset.item_id;
          const xmlhtt = new XMLHttpRequest();
          const url = `/api/unlike/${itemId}/`;
          xmlhtt.open("POST", url, true);
          xmlhtt.setRequestHeader("Content-Type", "application/json");
          xmlhtt.send(null);
          xmlhtt.onreadystatechange = () => {
              if (xmlhtt.readyState == 4 && xmlhtt.status == 200) {
                  try {
                      const jsonData = JSON.parse(xmlhtt.responseText);
                      if (jsonData.success) {
                          item.setAttribute("disabled", "disabled");
                          item.innerHTML = "Avgillat och klart";
                      } else {
                          console.error("Error occured with json data");
                          console.error(jsonData);
                      }
                  } catch (error) {
                      console.error("Error occured");
                      console.error(error);
                  }
              }
          };

      });
  });
});
