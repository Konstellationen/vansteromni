document.addEventListener('DOMContentLoaded', (e) => {
  // On page load or when changing themes, best to add inline in `head` to avoid FOUC
  if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
    document.documentElement.classList.add('dark')
  } else {
    document.documentElement.classList.remove('dark')
  }
});

let removeElement = (array, n) => {
  let newArray = [];

  for (let i = 0; i < array.length; i++) {
      if (array[i] !== n) {
          newArray.push(array[i]);
      }
  }
  return newArray;
};

window.addEventListener( "load", () => {
  // Load toggle states.
  let toggledAccordions =  JSON.parse(window.localStorage.getItem('toggledAccordions')) ?? [];
  toggledAccordions.forEach( ( toggledAccordion ) => {
      let accordions = document.querySelectorAll("[data-key='"+toggledAccordion+"']");
      accordions.forEach( (accordion) => {
          accordion.classList.add('closed');
      });
  });
  
  // Toggle and save states.
  const accordionToggles = document.querySelectorAll('.toggel-accordion');
  accordionToggles.forEach( (accordionToggle) => {
      accordionToggle.addEventListener( "click", (event) => {
          const parentAccordion = accordionToggle.closest('.accordion');
          const key = parentAccordion.dataset.key;
          parentAccordion.classList.toggle('closed');
          if(-1 === toggledAccordions.indexOf(key)) {
              toggledAccordions.push(key);
          } else {
              //toggledAccordions.splice(key);
              toggledAccordions = removeElement(toggledAccordions, key);
          }
          window.localStorage.setItem('toggledAccordions', JSON.stringify([...new Set(toggledAccordions)]));
      });
  });
  
  // Load viewed states.
  let viewedItems =  JSON.parse(window.localStorage.getItem('viewedItems')) ?? [];
  viewedItems.forEach( ( viewedItem ) => {
      let selectItems = document.querySelectorAll("[data-key='"+viewedItem+"']");
      selectItems.forEach( (selectItem) => {
          selectItem.classList.add('viewed');
      });
  });
  
  // Save viewed states.
  const items = document.querySelectorAll('.item');
  items.forEach( (item) => {
      const itemKey = item.dataset.key;
      if(itemKey){
          viewedItems.push(itemKey);
      }
  });
  window.localStorage.setItem('viewedItems', JSON.stringify([...new Set(viewedItems)]));
});
