from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify

# This is the model to describe how the rss/atom xml for a specific feed maps to Item
class ItemFeedMap(models.Model):
    item_feed_map_name = models.CharField(max_length=200)
    items = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    link = models.CharField(max_length=200)
    item_id = models.CharField(max_length=200)
    published = models.CharField(max_length=200)
    updated = models.CharField(max_length=200)
    content = models.CharField(max_length=200)
    summary = models.CharField(max_length=200)
    image_url = models.CharField(max_length=200)
    audio_url = models.CharField(max_length=200, default='')

    def __str__(self):
        return self.item_feed_map_name

class FeedCategory(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)  # optional description field
    slug = models.SlugField(null=False, blank=True, unique=True)

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)
    
    @property
    def all_feed_url(self):
        """
        Returns the hardcoded URL for the 'all' RSS feed for this category.
        """
        return f'/rss/{self.slug}/all/'

    @property
    def curated_feed_url(self):
        """
        Returns the hardcoded URL for the 'curated' RSS feed for this category.
        """
        return f'/rss/{self.slug}/curated/'


# This is the model for the feed, e.g. for https://konstellationen.org/atom.xml
class Feed(models.Model):
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    item_feed_map = models.ForeignKey(ItemFeedMap, on_delete=models.CASCADE)
    category = models.ForeignKey(FeedCategory, on_delete=models.SET_NULL, null=True, blank=True)
    political_affiliation = models.CharField(max_length=200, blank=True, null=True)
    publisher = models.CharField(max_length=200, blank=True, null=True)
    website_url = models.URLField(max_length=200, null=True, blank=True)
    mastodon_account_url = models.URLField(max_length=200, null=True, blank=True)
    slug = models.SlugField(null=False, blank=True, unique=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

# This describes a generic rss/atom feed item
class Item(models.Model):
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE)
    # These are things related to the moderation on this site
    date_processed = models.DateTimeField("date processed", null=True)
    show_item = models.BooleanField(default=False)
    imported = models.DateTimeField("date imported", null=True)

    # The following fields are fields taken from the rss/atom feed
    title = models.CharField(max_length=200)
    # An external link, to the news article, blog post, podcast, etc.
    link = models.CharField(max_length=200)
    item_id = models.CharField(max_length=200)
    published = models.DateTimeField("date published", null=True, blank=True)
    updated = models.DateTimeField("date updated", null=True, blank=True)
    content = models.TextField(blank=True)
    summary = models.TextField(blank=True)
    image_url = models.CharField(max_length=200, blank=True)
    audio_url = models.CharField(max_length=200, blank=True)
    
    def get_absolute_url(self):
        # Assuming you have a view named 'item_detail' that takes the item's ID as an argument
        return reverse('item_detail', args=[str(self.id)])

    def __str__(self):
        return "{} - {}".format(self.feed, self.title)
