data_konstellationen_feed_sample = """<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>Konstellationen</title>
  <icon>https://konstellationen.org/icon.png</icon>
  <subtitle>Kopplar samman vänstern</subtitle>
  <link href="https://konstellationen.org/atom.xml" rel="self"/>
  
  <link href="https://konstellationen.org/"/>
  <updated>2023-07-06T10:20:31.007Z</updated>
  <id>https://konstellationen.org/</id>
  
  <author>
    <name>Kamratdataföreningen Konstellationen</name>
    
  </author>
  
  <generator uri="https://hexo.io/">Hexo</generator>
  
  <entry>
    <title>Bortom Big Tech sociala medier - hur kan vänstern vara online?</title>
    <link href="https://konstellationen.org/2023/07/05/mastodonfest-rss-blogg/"/>
    <id>https://konstellationen.org/2023/07/05/mastodonfest-rss-blogg/</id>
    <published>2023-07-05T00:00:00.000Z</published>
    <updated>2023-07-06T10:20:31.007Z</updated>
    
    <content type="html"><![CDATA[<p>Välkommen på temakväll med efterhäng!</p><p>Vad finns för alternativ nu när allt fler sociala medier dör ut eller blir oanvändabara? Vad gör vänstern på internet? Lär dig om Fediversum och få hjälp att skapa konto på Mastodon. Delta i samtal om strategier för att nå ut utanför plattformarna, alternativ till sociala medier, bloggar, mail och poddar, samt käka, drick och häng med kamrater.</p><p><strong>Var:</strong> Solidaritetshuset på Södermalm, Stockholm<br><strong>När:</strong> Lördag 19 augusti kl 15:00-19:00<br>.</p>]]></content>
    
    
    <summary type="html">Välkommen på temakväll med efterhäng! Vad finns för alternativ nu när allt fler sociala medier dör ut eller blir oanvändabara? Vad gör vänstern på internet? Lär dig om Fediversum och få hjälp att skapa konto på Mastodon. Delta i samtal om strategier för att nå ut utanför plattformarna, alternativ till sociala medier, bloggar, mail och poddar, samt käka, drick och häng med kamrater.</summary>
    
    
    <content src="https://konstellationen.org/images/mastodonfest.jpg" type="image"/>
    
    
    
  </entry>

</feed>
"""

data_arbetaren_feed_sample = """<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	xmlns:dcterms="http://purl.org/dc/terms/" xmlns:media="http://search.yahoo.com/mrss/">

<channel>
	<title>Arbetaren</title>
	<atom:link href="https://www.arbetaren.se/feed/" rel="self" type="application/rss+xml" />
	<link>https://www.arbetaren.se</link>
	<description>En tidning på din sida</description>
	<lastBuildDate>Wed, 19 Jul 2023 07:49:27 +0000</lastBuildDate>
	<language>sv-SE</language>
	<sy:updatePeriod>
	hourly	</sy:updatePeriod>
	<sy:updateFrequency>
	1	</sy:updateFrequency>
	<generator>https://wordpress.org/?v=6.2.2</generator>

<image>
	<url>https://media.arbetaren.se/wp-content/uploads/2016/01/29031906/cropped-icon.png?width=64&#038;height=64</url>
	<title>Arbetaren</title>
	<link>https://www.arbetaren.se</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>Därför måste vi prata om sexbarnsfamiljernas lyxande</title>
		<link>https://www.arbetaren.se/2023/07/19/darfor-maste-vi-prata-om-sexbarnsfamiljernas-lyxande/</link>
		
		<dc:creator><![CDATA[Edvin Alpros]]></dc:creator>
		<pubDate>Wed, 19 Jul 2023 07:49:26 +0000</pubDate>
				<category><![CDATA[Satir]]></category>
		<guid isPermaLink="false">https://www.arbetaren.se/?p=316190</guid>

					<description><![CDATA[Edvin Alpros förklarar från sin segelbåt varför sexbarnsfamiljers lyxliv på existensminimum, män med smink och VAB-missbruket är de viktigaste debatterna för varje entreprenör att driva just nu.]]></description>
										<content:encoded><![CDATA[


<p class="has-preamble-font-size">Edvin Alpros förklarar från sin segelbåt varför sexbarnsfamiljers lyxliv på existensminimum, män med smink och VAB-missbruket är de viktigaste debatterna för varje entreprenör att driva just nu.</p>



<aside class="wp-block-arbetaren-aside">
<p><strong>Edvin Alpros</strong></p>



<p>Tredje namn M-listan* i Segelby. Bit coin forskare. Ordf. Kungabilens Vänner. Inspirations föreläsare. Diplomerad kurs deltagare.<br>(*Segelbys mans grupp)</p>



<p><em>Edvin Alpros – Brev från borgerligheten</em> är Arbetarens nya satrirvinjett, av skaparen bakom Twittersatirkontot Edvin Alpros.</p>
</aside>



<p>Sommaren är en tid för reflektion och jag känner mig tillfreds med året som gått. Jag har lanserat två nya företag, Segelby Cola och Segelby Crypto. Tack vare att ledningsgruppen (jag och min fru) ofta har ledningsgruppsmöten (semester) på min segelbåt på internationellt vatten så gäller inte längre svensk arbetsrätt för mina anställda, och detta har i sin tur gjort att vi kunnat lösgöra stora utgifter ifrån personalkostnaderna&nbsp;för att i stället kunna fokusera på&nbsp;kreativ bokföring. </p>



<p>Vi hämtar inspiration från entreprenörer inom andra branscher såsom taxiföretag utan anställda, skolkoncerner utan skolor och snabbmatsleverantörer utan fordon. I Sverige kan alla lyckas om man bara lägger manken till. Själv började jag med två tomma segelbåtar och ett mindre startkapital från min far och när jag nu&nbsp;30 år senare sitter i en av segelbåtarna och läser alarmistiska rubriker&nbsp;om ”global uppvärmning” samtidigt som jag blickar ut över vattenytan så slår det mig att om havsnivån faktiskt steg så hade ju jag förmodligen varit först med&nbsp;att upptäcka det.</p>


]]></content:encoded>
					
		
		
		<dcterms:publisher>http://id.kb.se/organisations/SE5565428413</dcterms:publisher>
<dcterms:accessRights>gratis</dcterms:accessRights>
<dcterms:format>text/html</dcterms:format>
<media:group>
<media:content url='https://media.arbetaren.se/wp-content/uploads/2023/07/18093936/Edvin-Alpros.jpg' type='image/jpeg'>
<dcterms:isFormatOf>
https://media.arbetaren.se/wp-content/uploads/2023/07/18093936/Edvin-Alpros.jpg</dcterms:isFormatOf>
</media:content>
</media:group>
	</item>
</channel>
</rss>
"""

data_etc_feed_sample = """<?xml version="1.0" encoding="utf-8" ?>
<rss version="2.0" xmlns:etcFeed="https://static.etc.se/etc-rss">
  <channel>
    <title>ETC.se</title>
    <link>https://www.etc.se/</link>
    <description>Röda tidningar för ett grönare Sverige!</description>
    <language>sv</language>
    <docs>https://validator.w3.org/feed/docs/rss2.html</docs>
    <webMaster>webmaster@etc.se (ETC Webmaster)</webMaster>
              <item>
        <title>Moderat toppnamn döms till fängelse efter flera våldsbrott</title>
        <link>https://www.etc.se/inrikes/moderat-toppnamn-doems-till-faengelse-efter-flera-vaaldsbrott?utm_source=rss</link>
        <description>&lt;p&gt;När han tog plats i kommunfullmäktige var han Moderaternas mest personkryssade i den mellansvenska kommunen. Men han hade också flera brott och ett fängelsestraff bakom sig. Nu har han återigen dömts – för en rad våldsbrott mot sin familj. &lt;/p&gt;
&lt;p&gt;– Jag har inbillat mig att alla föräldrar är så här, fast jag vet att det inte är så, säger en av hans döttrar i förhör.&lt;/p&gt;</description>
        <pubDate>Mon, 24 Jul 2023 14:00:00 +0200</pubDate>
        <guid>https://www.etc.se/inrikes/moderat-toppnamn-doems-till-faengelse-efter-flera-vaaldsbrott</guid>
                <author>Carolina Lundin</author>
                            <enclosure url="https://cdn.publisher-live.etc.nu/swp/uc3g8l/media/2023072412078_1d2f11930cfcbf3b2857d6d935bccab395a701b3cbec81ccc96f0e29ac72ccb0.png" length="772096" type="image/webp" />
                <etcFeed:order>0</etcFeed:order>
        <etcFeed:genre>Nyheter</etcFeed:genre>
        <etcFeed:section>Inrikes</etcFeed:section>
        <etcFeed:urgency>3</etcFeed:urgency>
      </item>
          <item>
        <title>Mariana på Rhodos: Det kommer ta veckor att släcka bränderna</title>
        <link>https://www.etc.se/utrikes/mariana-paa-rhodos-det-kommer-ta-veckor-att-slaecka-braenderna?utm_source=rss</link>
        <description>&lt;p&gt;I en vecka har Rhodos plågats av extremhetta och eldsvåda. Värdefull natur har totalförstörts och både turister och lokalbor har tvingats fly undan skogsbränderna, samtidigt som brandmän och volontärer arbetar dygnet runt för att släcka bränderna.&lt;/p&gt;
&lt;p&gt;– Människor är helt förtvivlade, säger svenska Marina Korkida, som har bott på Rhodos i över 25 år.&lt;/p&gt;</description>
        <pubDate>Mon, 24 Jul 2023 13:20:00 +0200</pubDate>
        <guid>https://www.etc.se/utrikes/mariana-paa-rhodos-det-kommer-ta-veckor-att-slaecka-braenderna</guid>
                <author>Hanna Strid</author>
                            <enclosure url="https://cdn.publisher-live.etc.nu/swp/uc3g8l/media/20230724110720_36535de4-7720-43e6-8bd2-ffb86f16d8bf.jpg" length="66560" type="image/jpeg" />
                <etcFeed:order>1</etcFeed:order>
        <etcFeed:genre>Nyheter</etcFeed:genre>
        <etcFeed:section>Utrikes</etcFeed:section>
        <etcFeed:urgency>3</etcFeed:urgency>
      </item>
	</channel>
</rss>
"""

data_aftonbladet_podcast_feed_sample = """<rss version="2.0">
    <channel>
        <title> Café Bambino </title>
        <description>
Tone Schunnesson hatar att ringas och Karin Pettersson vill inte skicka tusen sms. Så från och med nu ses dom en gång i veckan för att reda ut ett ämne, en gång för alla. Tillsammans med en inbjuden gäst undersöker dom stora saker som ilska, fiktion, skönhet och pengar. Och de går till botten med små, men viktiga frågor, som hur blir man arg utan att gråta? Dödade internet autofiktionen? Och varför är Karin fortfarande sosse? Två kompisar om internet, kultur, nöje, frihet och sanning. Två kompisar där den ena kompisen råkar vara den andra kompisens chef.
        </description>
        <language>sv</language>
        <link>https://play.acast.com/s/abpodcast</link>
        <copyright>Copyright: AB</copyright>
        <image>
            <url>
https://images.stream.schibsted.media/users/ab_/images/73a860b0f2db7cd684e0165b8baa9a18.jpg?t%5B%5D=3000x3000q80
            </url>
            <title> Café Bambino </title>
            <link>https://play.acast.com/s/abpodcast</link>
        </image>
        <itunes:image href="https://images.stream.schibsted.media/users/ab_/images/73a860b0f2db7cd684e0165b8baa9a18.jpg?t%5B%5D=3000x3000q80"/>
        <itunes:explicit>no</itunes:explicit>
        <itunes:author>Aftonbladet Kultur</itunes:author>
        <itunes:subtitle/>
        <itunes:summary>
Tone Schunnesson hatar att ringas och Karin Pettersson vill inte skicka tusen sms. Så från och med nu ses dom en gång i veckan för att reda ut ett ämne, en gång för alla. Tillsammans med en inbjuden gäst undersöker dom stora saker som ilska, fiktion, skönhet och pengar. Och de går till botten med små, men viktiga frågor, som hur blir man arg utan att gråta? Dödade internet autofiktionen? Och varför är Karin fortfarande sosse? Två kompisar om internet, kultur, nöje, frihet och sanning. Två kompisar där den ena kompisen råkar vara den andra kompisens chef.
        </itunes:summary>
        <itunes:type>episodic</itunes:type>
        <itunes:category text="Society & Culture">
            <itunes:category text="Personal Journals"/>
        </itunes:category>
        <itunes:owner>
            <itunes:name>Aftonbladet</itunes:name>
            <itunes:email>niklas.ornerud@aftonbladet.se</itunes:email>
        </itunes:owner>
        <atom:link rel="self" type="application/rss+xml" href="https://podcast.stream.schibsted.media/ab/1485"/>
        <generator>SVP Podcasts API</generator>
        <item>
            <title>
Festa med Henrik Brandão Jönsson och Victor Malms twitterfeed
            </title>
            <enclosure url="https://flex2.acast.com/s/cafe-bambino-podden/u/dd-ab.akamaized.net/ab/vod/2024/11/674845313234850008db53a2/podcast_128.mp3" length="43490048" type="audio/mpeg"/>
            <guid isPermaLink="false"> ab-377690 </guid>
            <acast:episodeId>ab-377690</acast:episodeId>
            <description>
Kära bambinos, i årets sista avsnitt går vi igenom lite saker vi inte fått sagt under hösten OCH vi har en julutlottning som heter duga. Tone sympatiserar med Henrik Brandão Jönsson, är trött på amerikanska poptjejer och vill inte dö hos gynekologen. Karin räknar tweets och undrar vad fan som menas med vänsterns Joe Rogan. Tack för en otrolig höst alla älskade bambinos, vi ses i slutet av januari! Följ oss på Instagram @cafebambino4ever för mer information om utlottning och livepodd! Den 30 januari kommer Café Bambino till Brogatan i Malmö och livepoddar. Nu kan du köpa din biljett här: https://billetto.se/e/livepod-cafe-bambino-biljetter-1123372?utm_source=organiser&utm_medium=share&utm_campaign=copy_link&utm_content=2
            </description>
            <pubDate>Fri, 29 Nov 2024 05:00:14 +0000</pubDate>
            <itunes:summary>
Kära bambinos, i årets sista avsnitt går vi igenom lite saker vi inte fått sagt under hösten OCH vi har en julutlottning som heter duga. Tone sympatiserar med Henrik Brandão Jönsson, är trött på amerikanska poptjejer och vill inte dö hos gynekologen. Karin räknar tweets och undrar vad fan som menas med vänsterns Joe Rogan. Tack för en otrolig höst alla älskade bambinos, vi ses i slutet av januari! Följ oss på Instagram @cafebambino4ever för mer information om utlottning och livepodd! Den 30 januari kommer Café Bambino till Brogatan i Malmö och livepoddar. Nu kan du köpa din biljett här: https://billetto.se/e/livepod-cafe-bambino-biljetter-1123372?utm_source=organiser&utm_medium=share&utm_campaign=copy_link&utm_content=2
            </itunes:summary>
            <itunes:duration>2718</itunes:duration>
            <itunes:image href="https://images.stream.schibsted.media/users/ab_/images/73a860b0f2db7cd684e0165b8baa9a18.jpg?t%5B%5D=1440q80"/>
            <itunes:subtitle>
Kära bambinos, i årets sista avsnitt går vi igenom lite saker vi inte fått sagt under hösten OCH vi har en julutlottning som heter duga. Tone sympatiserar med Henrik Brandão Jönsson, är trött på amerikanska poptjejer och vill inte dö hos gynekologen....
            </itunes:subtitle>
            <acast:settings>
eyJhZFNldHRpbmdzIjp7ImFkc0VuYWJsZWQiOnRydWUsInNwb25zRW5hYmxlZCI6dHJ1ZSwibnVtYmVyT2ZQcmVyb2xsQWRzIjoxLCJudW1iZXJPZlByZXJvbGxTcG9ucyI6MSwibnVtYmVyT2ZNaWRyb2xsQWRzIjoxLCJudW1iZXJPZk1pZHJvbGxTcG9ucyI6MSwibnVtYmVyT2ZQb3N0cm9sbEFkcyI6MSwibnVtYmVyT2ZQb3N0cm9sbFNwb25zIjoxLCJzbG90cyI6W3sidHlwZSI6InNwb25zIiwicGxhY2VtZW50IjoicHJlcm9sbCIsInN0YXJ0Ijo0ODksImR1cmF0aW9uIjo2MCwiY291bnQiOjF9LHsidHlwZSI6ImFkcyIsInBsYWNlbWVudCI6InByZXJvbGwiLCJzdGFydCI6NDg5LCJkdXJhdGlvbiI6NjAsImNvdW50IjoxfSx7InR5cGUiOiJzcG9ucyIsInBsYWNlbWVudCI6Im1pZHJvbGwiLCJzdGFydCI6MTUwMSwiZHVyYXRpb24iOjYwLCJjb3VudCI6MX0seyJ0eXBlIjoiYWRzIiwicGxhY2VtZW50IjoibWlkcm9sbCIsInN0YXJ0IjoxNTAxLCJkdXJhdGlvbiI6NjAsImNvdW50IjoxfSx7InR5cGUiOiJzcG9ucyIsInBsYWNlbWVudCI6InBvc3Ryb2xsIiwic3RhcnQiOjI3MTYsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9LHsidHlwZSI6ImFkcyIsInBsYWNlbWVudCI6InBvc3Ryb2xsIiwic3RhcnQiOjI3MTYsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9XSwicHJlcm9sbFNwb25zQmVmb3JlQWRzIjp0cnVlLCJtaWRyb2xsU3BvbnNCZWZvcmVBZHMiOnRydWV9fQ==
            </acast:settings>
        </item>
        <item>
            <title>
Vänsterpopulism, tyska konservativa kommunister och vad högern gör rätt
            </title>
            <enclosure url="https://flex2.acast.com/s/cafe-bambino-podden/u/dd-ab.akamaized.net/ab/vod/2024/11/673f6b7c3234850008db453d/podcast_128.mp3" length="48868736" type="audio/mpeg"/>
            <guid isPermaLink="false"> ab-377370 </guid>
            <acast:episodeId>ab-377370</acast:episodeId>
            <description>
I veckans avsnitt funderar vi på vänsterpopulism, ett par veckor efter att högerpopulismen vann valet i USA. Karin har fått dille på en ny, konstig politiker i Tyskland. Sahra Wagenknecht är kvinnan som på kort tid byggt ett anti-elitistiskt parti, döpt efter sig själv. Kärnväljarna består främst av arbetare, lågutbildade personer och personer med invandrarbakgrund, grupper som vänstern ständigt gnäller över att ha förlorat. Wagenknecht kommer från ett traditionellt vänsterhåll, men många av hennes värderingar är konservativa. Hon hatar cykelbanor och förespråkar en stram migrationspolitik, samtidigt som hon riktar stark kritik mot Israel och vill ha lägre energipriser. Flera framgångsrika politiker ritar numera sina egna politiska kartor utifrån väljarnas triggerpunkter och framförallt högern har lyckats identifiera dessa. Medan de bygger populistiska, politiska allianser haltar amerikanska liberaler och europeiska socialdemokrater långsamt efter. Varför då? När våras det egentligen för vänsterpopulismen? Är tyska Sahra Wagenknecht ett exempel på den? Vilka populistiska förslag går Tone till val på? Och så blir det tjafs om Richard Jomshof.
            </description>
            <pubDate>Fri, 22 Nov 2024 05:00:13 +0000</pubDate>
            <itunes:summary>
I veckans avsnitt funderar vi på vänsterpopulism, ett par veckor efter att högerpopulismen vann valet i USA. Karin har fått dille på en ny, konstig politiker i Tyskland. Sahra Wagenknecht är kvinnan som på kort tid byggt ett anti-elitistiskt parti, döpt efter sig själv. Kärnväljarna består främst av arbetare, lågutbildade personer och personer med invandrarbakgrund, grupper som vänstern ständigt gnäller över att ha förlorat. Wagenknecht kommer från ett traditionellt vänsterhåll, men många av hennes värderingar är konservativa. Hon hatar cykelbanor och förespråkar en stram migrationspolitik, samtidigt som hon riktar stark kritik mot Israel och vill ha lägre energipriser. Flera framgångsrika politiker ritar numera sina egna politiska kartor utifrån väljarnas triggerpunkter och framförallt högern har lyckats identifiera dessa. Medan de bygger populistiska, politiska allianser haltar amerikanska liberaler och europeiska socialdemokrater långsamt efter. Varför då? När våras det egentligen för vänsterpopulismen? Är tyska Sahra Wagenknecht ett exempel på den? Vilka populistiska förslag går Tone till val på? Och så blir det tjafs om Richard Jomshof.
            </itunes:summary>
            <itunes:duration>3054</itunes:duration>
            <itunes:image href="https://images.stream.schibsted.media/users/ab_/images/73a860b0f2db7cd684e0165b8baa9a18.jpg?t%5B%5D=1440q80"/>
            <itunes:subtitle>
I veckans avsnitt funderar vi på vänsterpopulism, ett par veckor efter att högerpopulismen vann valet i USA. Karin har fått dille på en ny, konstig politiker i Tyskland. Sahra Wagenknecht är kvinnan som på kort tid byggt ett anti-elitistiskt parti, d...
            </itunes:subtitle>
            <acast:settings>
eyJhZFNldHRpbmdzIjp7ImFkc0VuYWJsZWQiOnRydWUsInNwb25zRW5hYmxlZCI6dHJ1ZSwibnVtYmVyT2ZQcmVyb2xsQWRzIjoxLCJudW1iZXJPZlByZXJvbGxTcG9ucyI6MSwibnVtYmVyT2ZNaWRyb2xsQWRzIjoxLCJudW1iZXJPZk1pZHJvbGxTcG9ucyI6MSwibnVtYmVyT2ZQb3N0cm9sbEFkcyI6MSwibnVtYmVyT2ZQb3N0cm9sbFNwb25zIjoxLCJzbG90cyI6W3sidHlwZSI6InNwb25zIiwicGxhY2VtZW50IjoicHJlcm9sbCIsInN0YXJ0Ijo1MjgsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9LHsidHlwZSI6ImFkcyIsInBsYWNlbWVudCI6InByZXJvbGwiLCJzdGFydCI6NTI4LCJkdXJhdGlvbiI6NjAsImNvdW50IjoxfSx7InR5cGUiOiJzcG9ucyIsInBsYWNlbWVudCI6Im1pZHJvbGwiLCJzdGFydCI6MTc2OCwiZHVyYXRpb24iOjYwLCJjb3VudCI6MX0seyJ0eXBlIjoiYWRzIiwicGxhY2VtZW50IjoibWlkcm9sbCIsInN0YXJ0IjoxNzY4LCJkdXJhdGlvbiI6NjAsImNvdW50IjoxfSx7InR5cGUiOiJzcG9ucyIsInBsYWNlbWVudCI6InBvc3Ryb2xsIiwic3RhcnQiOjMwNTIsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9LHsidHlwZSI6ImFkcyIsInBsYWNlbWVudCI6InBvc3Ryb2xsIiwic3RhcnQiOjMwNTIsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9XSwicHJlcm9sbFNwb25zQmVmb3JlQWRzIjp0cnVlLCJtaWRyb2xsU3BvbnNCZWZvcmVBZHMiOnRydWV9fQ==
            </acast:settings>
        </item>
        <item>
            <title>
Simpa för män, ta dyra lån, hantera rasistiska kollegor
            </title>
            <enclosure url="https://flex2.acast.com/s/cafe-bambino-podden/u/dd-ab.akamaized.net/ab/vod/2024/11/6735c5e21a41330008443ce9/podcast_128.mp3" length="43519995" type="audio/mpeg"/>
            <guid isPermaLink="false"> ab-376991 </guid>
            <acast:episodeId>ab-376991</acast:episodeId>
            <description>
Frågelådan är öppen kära bambinos, eller snarare: Den VAR öppen och här kommer resultatet. Tack för alla superbra frågor och vi önskar vi kunnat svara på alla! Här blir det en blandat kompott av hur man dealar med hemska kollegor, vad man ska lägga sina pengar på, kan Tone tänka sig att simpa för en man och blir Karin och Tone ovänner?? Är det bra att identifiera sig med sin neuropsykiatriska diagnos? Vad gör Karin ängslig? Och har hon ens en tv hemma????
            </description>
            <pubDate>Fri, 15 Nov 2024 05:00:15 +0000</pubDate>
            <itunes:summary>
Frågelådan är öppen kära bambinos, eller snarare: Den VAR öppen och här kommer resultatet. Tack för alla superbra frågor och vi önskar vi kunnat svara på alla! Här blir det en blandat kompott av hur man dealar med hemska kollegor, vad man ska lägga sina pengar på, kan Tone tänka sig att simpa för en man och blir Karin och Tone ovänner?? Är det bra att identifiera sig med sin neuropsykiatriska diagnos? Vad gör Karin ängslig? Och har hon ens en tv hemma????
            </itunes:summary>
            <itunes:duration>2720</itunes:duration>
            <itunes:image href="https://images.stream.schibsted.media/users/ab_/images/73a860b0f2db7cd684e0165b8baa9a18.jpg?t%5B%5D=1440q80"/>
            <itunes:subtitle>
Frågelådan är öppen kära bambinos, eller snarare: Den VAR öppen och här kommer resultatet. Tack för alla superbra frågor och vi önskar vi kunnat svara på alla! Här blir det en blandat kompott av hur man dealar med hemska kollegor, vad man ska lägga s...
            </itunes:subtitle>
            <acast:settings>
eyJhZFNldHRpbmdzIjp7ImFkc0VuYWJsZWQiOnRydWUsInNwb25zRW5hYmxlZCI6dHJ1ZSwibnVtYmVyT2ZQcmVyb2xsQWRzIjoxLCJudW1iZXJPZlByZXJvbGxTcG9ucyI6MSwibnVtYmVyT2ZNaWRyb2xsQWRzIjoxLCJudW1iZXJPZk1pZHJvbGxTcG9ucyI6MSwibnVtYmVyT2ZQb3N0cm9sbEFkcyI6MSwibnVtYmVyT2ZQb3N0cm9sbFNwb25zIjoxLCJzbG90cyI6W3sidHlwZSI6InNwb25zIiwicGxhY2VtZW50IjoicHJlcm9sbCIsInN0YXJ0Ijo0MzQsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9LHsidHlwZSI6ImFkcyIsInBsYWNlbWVudCI6InByZXJvbGwiLCJzdGFydCI6NDM0LCJkdXJhdGlvbiI6NjAsImNvdW50IjoxfSx7InR5cGUiOiJzcG9ucyIsInBsYWNlbWVudCI6Im1pZHJvbGwiLCJzdGFydCI6MTgwMCwiZHVyYXRpb24iOjYwLCJjb3VudCI6MX0seyJ0eXBlIjoiYWRzIiwicGxhY2VtZW50IjoibWlkcm9sbCIsInN0YXJ0IjoxODAwLCJkdXJhdGlvbiI6NjAsImNvdW50IjoxfSx7InR5cGUiOiJzcG9ucyIsInBsYWNlbWVudCI6InBvc3Ryb2xsIiwic3RhcnQiOjI3MTgsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9LHsidHlwZSI6ImFkcyIsInBsYWNlbWVudCI6InBvc3Ryb2xsIiwic3RhcnQiOjI3MTgsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9XSwicHJlcm9sbFNwb25zQmVmb3JlQWRzIjp0cnVlLCJtaWRyb2xsU3BvbnNCZWZvcmVBZHMiOnRydWV9fQ==
            </acast:settings>
        </item>
        <item>
            <title>
Amer Sarsour om prosans magi, flyktens odyssé och att bli någon att räkna med
            </title>
            <enclosure url="https://flex2.acast.com/s/cafe-bambino-podden/u/dd-ab.akamaized.net/ab/vod/2024/11/672c8ba31108500008e08517/podcast_128.mp3" length="60550391" type="audio/mpeg"/>
            <guid isPermaLink="false"> ab-376752 </guid>
            <acast:episodeId>ab-376752</acast:episodeId>
            <description>
I veckans avsnitt gästas vi av den hyllade författaren Amer Sarsour och har ett riktig kanonbra författarsamtal. Amer är en etablerad poet och musiker som i våras romandebuterade med magiska ”Medan vi brinner”! Berättelsen har han burit med sig i tio år. Romanen är en gripande och spännande historia om tjugotreåriga Omar som har tagit på sig ett viktigt uppdrag för att hjälpa sin pappa. Han ska åka ner till södra Italien och hämta sina kusiner. Året är 2013, de har flytt det brinnande Syrien, korsat havet och tagit sig till Europa. Nu ska Omar hjälpa dem den sista biten på vägen, hem till tryggheten i Sverige. Men resan visar sig vara svårare än han trott. Det blir ett samtal om hur man blir en pålitlig person och sökandet efter en berättelses form. Amer berättar om att hitta sin romanfigurs unika röst, med hjälp av landmärken i Uppsala. Vad är skillnaden mellan självförverkligande och meningsskapande? Går det att prata politik med känslor, varför skulle förväntningar vara dåligt och hur kan man förstå Sverige på ett mänskligt plan? Kan kampen som gör så ont skrivas vackert? Och hur jävla stort är egentligen Europa?
            </description>
            <pubDate>Fri, 08 Nov 2024 05:00:07 +0000</pubDate>
            <itunes:summary>
I veckans avsnitt gästas vi av den hyllade författaren Amer Sarsour och har ett riktig kanonbra författarsamtal. Amer är en etablerad poet och musiker som i våras romandebuterade med magiska ”Medan vi brinner”! Berättelsen har han burit med sig i tio år. Romanen är en gripande och spännande historia om tjugotreåriga Omar som har tagit på sig ett viktigt uppdrag för att hjälpa sin pappa. Han ska åka ner till södra Italien och hämta sina kusiner. Året är 2013, de har flytt det brinnande Syrien, korsat havet och tagit sig till Europa. Nu ska Omar hjälpa dem den sista biten på vägen, hem till tryggheten i Sverige. Men resan visar sig vara svårare än han trott. Det blir ett samtal om hur man blir en pålitlig person och sökandet efter en berättelses form. Amer berättar om att hitta sin romanfigurs unika röst, med hjälp av landmärken i Uppsala. Vad är skillnaden mellan självförverkligande och meningsskapande? Går det att prata politik med känslor, varför skulle förväntningar vara dåligt och hur kan man förstå Sverige på ett mänskligt plan? Kan kampen som gör så ont skrivas vackert? Och hur jävla stort är egentligen Europa?
            </itunes:summary>
            <itunes:duration>3784</itunes:duration>
            <itunes:image href="https://images.stream.schibsted.media/users/ab_/images/73a860b0f2db7cd684e0165b8baa9a18.jpg?t%5B%5D=1440q80"/>
            <itunes:subtitle>
I veckans avsnitt gästas vi av den hyllade författaren Amer Sarsour och har ett riktig kanonbra författarsamtal. Amer är en etablerad poet och musiker som i våras romandebuterade med magiska ”Medan vi brinner”! Berättelsen har han burit med sig i tio...
            </itunes:subtitle>
            <acast:settings>
eyJhZFNldHRpbmdzIjp7ImFkc0VuYWJsZWQiOnRydWUsInNwb25zRW5hYmxlZCI6dHJ1ZSwibnVtYmVyT2ZQcmVyb2xsQWRzIjoxLCJudW1iZXJPZlByZXJvbGxTcG9ucyI6MSwibnVtYmVyT2ZNaWRyb2xsQWRzIjoxLCJudW1iZXJPZk1pZHJvbGxTcG9ucyI6MSwibnVtYmVyT2ZQb3N0cm9sbEFkcyI6MSwibnVtYmVyT2ZQb3N0cm9sbFNwb25zIjoxLCJzbG90cyI6W3sidHlwZSI6InNwb25zIiwicGxhY2VtZW50IjoicHJlcm9sbCIsInN0YXJ0IjoxMDA1LCJkdXJhdGlvbiI6NjAsImNvdW50IjoxfSx7InR5cGUiOiJhZHMiLCJwbGFjZW1lbnQiOiJwcmVyb2xsIiwic3RhcnQiOjEwMDUsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9LHsidHlwZSI6InNwb25zIiwicGxhY2VtZW50IjoibWlkcm9sbCIsInN0YXJ0IjoyNDg4LCJkdXJhdGlvbiI6NjAsImNvdW50IjoxfSx7InR5cGUiOiJhZHMiLCJwbGFjZW1lbnQiOiJtaWRyb2xsIiwic3RhcnQiOjI0ODgsImR1cmF0aW9uIjo2MCwiY291bnQiOjF9LHsidHlwZSI6InNwb25zIiwicGxhY2VtZW50IjoicG9zdHJvbGwiLCJzdGFydCI6Mzc4MCwiZHVyYXRpb24iOjYwLCJjb3VudCI6MX0seyJ0eXBlIjoiYWRzIiwicGxhY2VtZW50IjoicG9zdHJvbGwiLCJzdG∏FydCI6Mzc4MCwiZHVyYXRpb24iOjYwLCJjb3VudCI6MX1dLCJwcmVyb2xsU3BvbnNCZWZvcmVBZHMiOnRydWUsIm1pZHJvbGxTcG9uc0JlZm9yZUFkcyI6dHJ1ZX19
            </acast:settings>
        </item>
    </channel>
</rss>
"""

data_studio_expo_podcast_libsyn_feed_sample = """<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:cc="http://web.resource.org/cc/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:media="http://search.yahoo.com/mrss/" xmlns:content="http://purl.org/rss/1.0/modules/content/"  xmlns:podcast="https://podcastindex.org/namespace/1.0"  xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
	<channel>
		<atom:link href="https://feeds.libsyn.com/412148/rss" rel="self" type="application/rss+xml"/>
		<title>Studio Expo</title>
		<pubDate>Fri, 20 Dec 2024 04:30:00 +0000</pubDate>
		<lastBuildDate>Mon, 23 Dec 2024 14:05:27 +0000</lastBuildDate>
		<generator>Libsyn WebEngine 2.0</generator>
		<link>http://expo.se</link>
		<language>sv</language>
		<copyright><![CDATA[© Stiftelsen Expo 2024]]></copyright>
		<docs>http://expo.se</docs>
		<managingEditor>anna.frojd@expo.se (anna.frojd@expo.se)</managingEditor>
		<itunes:summary><![CDATA[Varje vecka bjuder vi på fördjupning och analys med utgångspunkt i egna publiceringar och andra viktiga nyhetshändelser inom Expos bevakningsområde. Ansvarig utgivare: Daniel Poohl]]></itunes:summary>
		<image>
			<url>https://static.libsyn.com/p/assets/e/6/5/6/e656a042b1125e3427a2322813b393ee/Studio_Expo_cover_23.jpg</url>
			<title>Studio Expo</title>
			<link><![CDATA[http://expo.se]]></link>
		</image>
		<itunes:author>Stiftelsen Expo</itunes:author>
		<itunes:category text="News">
			<itunes:category text="Politics"/>
			<itunes:category text="News Commentary"/>
		</itunes:category>
		<itunes:image href="https://static.libsyn.com/p/assets/e/6/5/6/e656a042b1125e3427a2322813b393ee/Studio_Expo_cover_23.jpg" />
		<itunes:explicit>false</itunes:explicit>
		<itunes:owner>
			<itunes:name><![CDATA[Stiftelsen Expo]]></itunes:name>
			<itunes:email>anna.frojd@expo.se</itunes:email>
		</itunes:owner>
		<description><![CDATA[Varje vecka bjuder vi på fördjupning och analys med utgångspunkt i egna publiceringar och andra viktiga nyhetshändelser inom Expos bevakningsområde. Ansvarig utgivare: Daniel Poohl]]></description>
		<itunes:type>episodic</itunes:type>
		


		<podcast:locked owner="anna.frojd@expo.se">no</podcast:locked>
		
		<item>
			<title>135. 100 år av svensk nazism: Del 4 av 4 – Gatuaktivisterna</title>
			<itunes:title>100 år av svensk nazism: Del 4 av 4 – Gatuaktivisterna</itunes:title>
			<pubDate>Fri, 20 Dec 2024 04:30:00 +0000</pubDate>
			<guid isPermaLink="false"><![CDATA[57720263-9d2d-4c72-b869-2faff16a2f2f]]></guid>
			<link><![CDATA[https://sites.libsyn.com/412148/135-100-r-av-svensk-nazism-del-4-av-4-gatuaktivisterna]]></link>
			<itunes:image href="https://static.libsyn.com/p/assets/2/3/f/1/23f122d583a7724fd959afa2a1bf1c87/Kopia_av_MALL_Studio_Expo_Cover_Sommar_Spotify_5.jpg" />
			<description><![CDATA[<p>I år är det 100 år sedan det första svenska nazistpartiet bildades. Trots vetskapen om nazismens fruktansvärda konsekvenser har nya anhängare fortsatt att attraheras av dess rasism och antidemokratiska idéer. 100 år av obruten nazistisk organisering. Det är en brokig historia fylld av våld, terror och gatuaktivism. Men även valsatsningar och entreprenörskap. Mängder av partier och organisationer har bildats och försvunnit. Det har varit otaliga interna strider och fraktionsbildningar, men även återkommande enhetsförsök. Expo kommer i fyra avsnitt att berätta om den här heterogena rörelsens utveckling och olika faser från de första partibildningarna till dagens slagsmålsklubbar.  </p> <p><mark class="has-inline-color has-black-color">Vi har nu kommit fram till</mark> 2000-talet. Ett mord i Salem som får stora delar av den svenska nazistmiljön att enas och under det nya millenniets första decennium genomförs flera samlande demonstrationer innan konflikter och interna strider återigen sätter stopp för sammanhållningen. </p> <p>Det fjärde och avslutande avsnittet i serien om 100 år av svensk nazism handlar om vår samtida nazistiska historia — där det parallellt med partisatsningar går att se hur lösare nätverk och lokala aktivistgrupper slår igenom. Det är en trendkänslig, internationellt uppkopplad och snabbt skiftande miljö som jämte nazismen ger plats för identitärer, nyfascister, fria nationalister, realister, alt-right-aktivister, medborgarjournalister och aktivklubbar. Och återigen är våldet den nazistiska idévärldens följeslagare — med bombattentat, mord och skoldåd. </p> <p><mark class="has-inline-color has-black-color">Hur kan vi förstå det myller</mark> av organisationsformer vi ser växa fram under 2000-talet? Hur utnyttjar extremhögern den digitala revolutionen? Och hur påverkar miljöns nu nästan 100 åriga historia den samtida nazismen? Vi är framme vid smörgåsbordsnazismen och de så kallade ensamagerande terroristernas epok.</p> <p>Gäster i studion är Daniel Poohl, vd på Expo och Anders Dalsbro, reporter på Expo.</p> <p>Programledare: Anna Fröjd</p> <p dir="ltr">---</p> <p dir="ltr"><strong>Läs mer:</strong></p> <p>Studio Expo: 100 år av svensk nazism – Del 1 av 4: Pionjärerna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/</a> </p> <p>Studio Expo: 100 år av svensk nazism – Del 2 av 4: Övervintrarna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/</a></p> <p>Studio Expo: 100 år av svensk nazism – del 3 av 4: Raskrigarna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-raskrigarna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-raskrigarna/</a> </p> <p>#4-2023 Tidskriften Expo, 100 år av svensk nazism: <a href= "https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/">https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/</a> </p> <p>Expo wiki – Nationalsocialistisk front (NSF): <a href= "https://expo.se/lar-dig-mer/wiki/nationalsocialistisk-front-nsf/">https://expo.se/lar-dig-mer/wiki/nationalsocialistisk-front-nsf/</a> </p> <p>Expo wiki – Nordiska förbundet: <a href= "https://expo.se/lar-dig-mer/wiki/nordiska-forbundet/">https://expo.se/lar-dig-mer/wiki/nordiska-forbundet/</a> </p> <p>Expo wiki – Nationaldemokraterna: <a href= "https://expo.se/lar-dig-mer/wiki/nationaldemokraterna/">https://expo.se/lar-dig-mer/wiki/nationaldemokraterna/</a> </p> <p>Expo wiki – Nordiska motståndsrörelsen (NMR): <a href= "https://expo.se/lar-dig-mer/wiki/nordiska-motstandsrorelsen-nmr/">https://expo.se/lar-dig-mer/wiki/nordiska-motstandsrorelsen-nmr/</a> </p> <p>Expo wiki – Alt-right: <a href= "https://expo.se/lar-dig-mer/wiki/alt-right-rorelsen/">https://expo.se/lar-dig-mer/wiki/alt-right-rorelsen/</a> </p> <p> </p> <p dir="ltr"><strong><span id= "docs-internal-guid-e46c9aa8-7fff-473e-365f-a511c6ad2020">Expo behöver ditt stöd</span></strong></p> <p dir="ltr">Bli poddvän här: <a href= "https://expo.se/stod-expo/manadsgivare/">https://expo.se/stod-expo/bli-poddvan/</a></p> <p dir="ltr">Prenumerera på Expo: <a href= "https://expo.se/tidskriften/prenumerera">https://expo.se/tidskriften/prenumerera</a> </p> <p>---</p> <p><strong>Studio Expo ger dig</strong> som lyssnar fördjupningar om våra avslöjanden, mer om våra granskningar och analyser av högextrema tendenser. Varje vecka i din poddspelare!</p> <p>Expo är en religiöst och partipolitiskt obunden stiftelse. Vi har granskat och bevakat extremhögern sedan 1995 – för en levande demokrati där rasistiska idéer och organisationer saknar inflytande.</p>]]></description>
			<content:encoded><![CDATA[<p>I år är det 100 år sedan det första svenska nazistpartiet bildades. Trots vetskapen om nazismens fruktansvärda konsekvenser har nya anhängare fortsatt att attraheras av dess rasism och antidemokratiska idéer. 100 år av obruten nazistisk organisering. Det är en brokig historia fylld av våld, terror och gatuaktivism. Men även valsatsningar och entreprenörskap. Mängder av partier och organisationer har bildats och försvunnit. Det har varit otaliga interna strider och fraktionsbildningar, men även återkommande enhetsförsök. Expo kommer i fyra avsnitt att berätta om den här heterogena rörelsens utveckling och olika faser från de första partibildningarna till dagens slagsmålsklubbar.  </p> <p>Vi har nu kommit fram till 2000-talet. Ett mord i Salem som får stora delar av den svenska nazistmiljön att enas och under det nya millenniets första decennium genomförs flera samlande demonstrationer innan konflikter och interna strider återigen sätter stopp för sammanhållningen. </p> <p>Det fjärde och avslutande avsnittet i serien om 100 år av svensk nazism handlar om vår samtida nazistiska historia — där det parallellt med partisatsningar går att se hur lösare nätverk och lokala aktivistgrupper slår igenom. Det är en trendkänslig, internationellt uppkopplad och snabbt skiftande miljö som jämte nazismen ger plats för identitärer, nyfascister, fria nationalister, realister, alt-right-aktivister, medborgarjournalister och aktivklubbar. Och återigen är våldet den nazistiska idévärldens följeslagare — med bombattentat, mord och skoldåd. </p> <p>Hur kan vi förstå det myller av organisationsformer vi ser växa fram under 2000-talet? Hur utnyttjar extremhögern den digitala revolutionen? Och hur påverkar miljöns nu nästan 100 åriga historia den samtida nazismen? Vi är framme vid smörgåsbordsnazismen och de så kallade ensamagerande terroristernas epok.</p> <p>Gäster i studion är Daniel Poohl, vd på Expo och Anders Dalsbro, reporter på Expo.</p> <p>Programledare: Anna Fröjd</p> <p dir="ltr">---</p> <p dir="ltr">Läs mer:</p> <p>Studio Expo: 100 år av svensk nazism – Del 1 av 4: Pionjärerna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/</a> </p> <p>Studio Expo: 100 år av svensk nazism – Del 2 av 4: Övervintrarna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/</a></p> <p>Studio Expo: 100 år av svensk nazism – del 3 av 4: Raskrigarna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-raskrigarna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-raskrigarna/</a> </p> <p>#4-2023 Tidskriften Expo, 100 år av svensk nazism: <a href= "https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/">https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/</a> </p> <p>Expo wiki – Nationalsocialistisk front (NSF): <a href= "https://expo.se/lar-dig-mer/wiki/nationalsocialistisk-front-nsf/">https://expo.se/lar-dig-mer/wiki/nationalsocialistisk-front-nsf/</a> </p> <p>Expo wiki – Nordiska förbundet: <a href= "https://expo.se/lar-dig-mer/wiki/nordiska-forbundet/">https://expo.se/lar-dig-mer/wiki/nordiska-forbundet/</a> </p> <p>Expo wiki – Nationaldemokraterna: <a href= "https://expo.se/lar-dig-mer/wiki/nationaldemokraterna/">https://expo.se/lar-dig-mer/wiki/nationaldemokraterna/</a> </p> <p>Expo wiki – Nordiska motståndsrörelsen (NMR): <a href= "https://expo.se/lar-dig-mer/wiki/nordiska-motstandsrorelsen-nmr/">https://expo.se/lar-dig-mer/wiki/nordiska-motstandsrorelsen-nmr/</a> </p> <p>Expo wiki – Alt-right: <a href= "https://expo.se/lar-dig-mer/wiki/alt-right-rorelsen/">https://expo.se/lar-dig-mer/wiki/alt-right-rorelsen/</a> </p> <p> </p> <p dir="ltr">Expo behöver ditt stöd</p> <p dir="ltr">Bli poddvän här: <a href= "https://expo.se/stod-expo/manadsgivare/">https://expo.se/stod-expo/bli-poddvan/</a></p> <p dir="ltr">Prenumerera på Expo: <a href= "https://expo.se/tidskriften/prenumerera">https://expo.se/tidskriften/prenumerera</a> </p> <p>---</p> <p>Studio Expo ger dig som lyssnar fördjupningar om våra avslöjanden, mer om våra granskningar och analyser av högextrema tendenser. Varje vecka i din poddspelare!</p> <p>Expo är en religiöst och partipolitiskt obunden stiftelse. Vi har granskat och bevakat extremhögern sedan 1995 – för en levande demokrati där rasistiska idéer och organisationer saknar inflytande.</p>]]></content:encoded>
			<enclosure length="65348903" type="audio/mpeg" url="https://traffic.libsyn.com/secure/cfd1d86e-6f6c-4f03-b723-512a2bdd4b90/135._100_ar_av_svensk_nazism_del_4_av_4__Gatuaktivisterna.mp3?dest-id=3415931" />
			<itunes:duration>01:08:05</itunes:duration>
			<itunes:explicit>false</itunes:explicit>
			<itunes:keywords />
			<itunes:subtitle><![CDATA[I år är det 100 år sedan det första svenska nazistpartiet bildades. Trots vetskapen om nazismens fruktansvärda konsekvenser har nya anhängare fortsatt att attraheras av dess rasism och antidemokratiska idéer. 100 år av obruten nazistisk...]]></itunes:subtitle>
			<itunes:episode>135</itunes:episode>
			<itunes:episodeType>full</itunes:episodeType>
		</item>
		<item>
			<title>134. 100 år av svensk nazism: Del 3 av 4 – Raskrigarna</title>
			<itunes:title>100 år av svensk nazism: Del 3 av 4 – Raskrigarna</itunes:title>
			<pubDate>Fri, 13 Dec 2024 04:30:00 +0000</pubDate>
			<guid isPermaLink="false"><![CDATA[4219ad73-cee9-43e3-b841-f91dddc0ebbc]]></guid>
			<link><![CDATA[https://sites.libsyn.com/412148/134-100-r-av-svensk-nazism-del-3-av-4-raskrigarna]]></link>
			<itunes:image href="https://static.libsyn.com/p/assets/b/4/b/6/b4b6ee787d83a112d959afa2a1bf1c87/Kopia_av_MALL_Studio_Expo_Cover_Sommar_Spotify_8-20241218-ysj0d9e2qb.jpg" />
			<description><![CDATA[<p>I år är det 100 år sedan det första svenska nazistpartiet bildades. Trots vetskapen om nazismens fruktansvärda konsekvenser har nya anhängare fortsatt att attraheras av dess rasism och antidemokratiska idéer. 100 år av obruten nazistisk organisering.</p> <p>Det är en brokig historia fylld av våld, terror och gatuaktivism. Men även valsatsningar och entreprenörskap. Mängder av partier och organisationer har bildats och försvunnit. Det har varit otaliga interna strider och fraktionsbildningar, men även återkommande enhetsförsök. Expo kommer i fyra avsnitt att berätta om den här heterogena rörelsens utveckling och olika faser från de första partibildningarna till dagens slagsmålsklubbar. </p> <p>I avsnitt tre har vi kommit fram till perioden 1980 till 1999. En tid när den nazistiska rörelsen genomgår en föryngring och radikalisering. Skinnskallar, vit makt-musik och drömmar om ett raskrig präglar miljön och de svenska nazisterna hämtar sin inspiration från förebilder i USA och Storbritannien. Perioden inleds med att flera nazistkopplade personer ställs inför rätta för en rad våldsdåd och hot och avslutas med fler uppmärksammade mord. </p> <p>Hur kom det sig att nazismens idéer återigen kunde locka unga? Hur kan vi förstå den radikalisering rörelsen genomgår? Och hur såg egentligen relationen ut mellan nazisterna, BSS och det nybildade Sverigedemokraterna?</p> <p>Gäster i studion är Anna-Lena Lodenius, journalist specialiserad på extremism och Daniel Poohl, vd på Expo. </p> <p>Programledare: Anna Fröjd</p> <p dir="ltr">---</p> <p dir="ltr"><strong>Läs mer:</strong></p> <p>Studio Expo: 100 år av svensk nazism – Del 1 av 4: Pionjärerna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/</a> </p> <p>Studio Expo: 100 år av svensk nazism – Del 2 av 4: Övervintrarna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/</a></p> <p>#4-2023 Tidskriften Expo, 100 år av svensk nazism: <a href= "https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/">https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/</a> </p> <p>Mytkollen: Våldsprofet för en hotad vithet (Vergara, 2019): <a href= "https://expo.se/fordjupning/sjalvforsvarsprofet-for-en-hotad-vithet/"> https://expo.se/fordjupning/sjalvforsvarsprofet-for-en-hotad-vithet/</a></p> <p>Mytkollen: Myten om ett folkmord på vita har blivit ett mantra (Vergara, 2019): <a href= "https://expo.se/nyhet/folkmordet-har-blivit-ett-mantra/">https://expo.se/nyhet/folkmordet-har-blivit-ett-mantra/</a></p> <p>Salemmarschen år för år: <a href= "https://expo.se/nyhet/salemmarschen-ar-for-ar/">https://expo.se/nyhet/salemmarschen-ar-for-ar/</a></p> <p>Expo wiki - Vitt ariskt motstånd: <a href= "https://expo.se/lar-dig-mer/wiki/vitt-ariskt-motstand-vam/">https://expo.se/lar-dig-mer/wiki/vitt-ariskt-motstand-vam/</a></p> <p>Expo wiki - Turner diaries: <a href= "https://expo.se/lar-dig-mer/wiki/turner-diaries/">https://expo.se/lar-dig-mer/wiki/turner-diaries/</a></p> <p>Expo wiki - Ledarlöst motstånd: <a href= "https://expo.se/lar-dig-mer/wiki/ledarlost-motstand/">https://expo.se/lar-dig-mer/wiki/ledarlost-motstand/</a></p> <p>Morden i Malexander en symbol för polishat (Sidenbladh, 2021): <a href= "https://expo.se/nyhet/morden-i-malexander-en-symbol-for-polishat/"> https://expo.se/nyhet/morden-i-malexander-en-symbol-for-polishat/</a></p> <p> </p> <p dir="ltr"><strong><span id= "docs-internal-guid-e46c9aa8-7fff-473e-365f-a511c6ad2020">Expo behöver ditt stöd</span></strong></p> <p dir="ltr">Bli poddvän här: <a href= "https://expo.se/stod-expo/manadsgivare/">https://expo.se/stod-expo/bli-poddvan/</a></p> <p dir="ltr">Prenumerera på Expo: <a href= "https://expo.se/tidskriften/prenumerera">https://expo.se/tidskriften/prenumerera</a> </p> <p>---</p> <p><strong>Studio Expo ger dig</strong> som lyssnar fördjupningar om våra avslöjanden, mer om våra granskningar och analyser av högextrema tendenser. Varje vecka i din poddspelare!</p> <p>Expo är en religiöst och partipolitiskt obunden stiftelse. Vi har granskat och bevakat extremhögern sedan 1995 – för en levande demokrati där rasistiska idéer och organisationer saknar inflytande.</p>]]></description>
			<content:encoded><![CDATA[<p>I år är det 100 år sedan det första svenska nazistpartiet bildades. Trots vetskapen om nazismens fruktansvärda konsekvenser har nya anhängare fortsatt att attraheras av dess rasism och antidemokratiska idéer. 100 år av obruten nazistisk organisering.</p> <p>Det är en brokig historia fylld av våld, terror och gatuaktivism. Men även valsatsningar och entreprenörskap. Mängder av partier och organisationer har bildats och försvunnit. Det har varit otaliga interna strider och fraktionsbildningar, men även återkommande enhetsförsök. Expo kommer i fyra avsnitt att berätta om den här heterogena rörelsens utveckling och olika faser från de första partibildningarna till dagens slagsmålsklubbar. </p> <p>I avsnitt tre har vi kommit fram till perioden 1980 till 1999. En tid när den nazistiska rörelsen genomgår en föryngring och radikalisering. Skinnskallar, vit makt-musik och drömmar om ett raskrig präglar miljön och de svenska nazisterna hämtar sin inspiration från förebilder i USA och Storbritannien. Perioden inleds med att flera nazistkopplade personer ställs inför rätta för en rad våldsdåd och hot och avslutas med fler uppmärksammade mord. </p> <p>Hur kom det sig att nazismens idéer återigen kunde locka unga? Hur kan vi förstå den radikalisering rörelsen genomgår? Och hur såg egentligen relationen ut mellan nazisterna, BSS och det nybildade Sverigedemokraterna?</p> <p>Gäster i studion är Anna-Lena Lodenius, journalist specialiserad på extremism och Daniel Poohl, vd på Expo. </p> <p>Programledare: Anna Fröjd</p> <p dir="ltr">---</p> <p dir="ltr">Läs mer:</p> <p>Studio Expo: 100 år av svensk nazism – Del 1 av 4: Pionjärerna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/</a> </p> <p>Studio Expo: 100 år av svensk nazism – Del 2 av 4: Övervintrarna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/</a></p> <p>#4-2023 Tidskriften Expo, 100 år av svensk nazism: <a href= "https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/">https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/</a> </p> <p>Mytkollen: Våldsprofet för en hotad vithet (Vergara, 2019): <a href= "https://expo.se/fordjupning/sjalvforsvarsprofet-for-en-hotad-vithet/"> https://expo.se/fordjupning/sjalvforsvarsprofet-for-en-hotad-vithet/</a></p> <p>Mytkollen: Myten om ett folkmord på vita har blivit ett mantra (Vergara, 2019): <a href= "https://expo.se/nyhet/folkmordet-har-blivit-ett-mantra/">https://expo.se/nyhet/folkmordet-har-blivit-ett-mantra/</a></p> <p>Salemmarschen år för år: <a href= "https://expo.se/nyhet/salemmarschen-ar-for-ar/">https://expo.se/nyhet/salemmarschen-ar-for-ar/</a></p> <p>Expo wiki - Vitt ariskt motstånd: <a href= "https://expo.se/lar-dig-mer/wiki/vitt-ariskt-motstand-vam/">https://expo.se/lar-dig-mer/wiki/vitt-ariskt-motstand-vam/</a></p> <p>Expo wiki - Turner diaries: <a href= "https://expo.se/lar-dig-mer/wiki/turner-diaries/">https://expo.se/lar-dig-mer/wiki/turner-diaries/</a></p> <p>Expo wiki - Ledarlöst motstånd: <a href= "https://expo.se/lar-dig-mer/wiki/ledarlost-motstand/">https://expo.se/lar-dig-mer/wiki/ledarlost-motstand/</a></p> <p>Morden i Malexander en symbol för polishat (Sidenbladh, 2021): <a href= "https://expo.se/nyhet/morden-i-malexander-en-symbol-for-polishat/"> https://expo.se/nyhet/morden-i-malexander-en-symbol-for-polishat/</a></p> <p> </p> <p dir="ltr">Expo behöver ditt stöd</p> <p dir="ltr">Bli poddvän här: <a href= "https://expo.se/stod-expo/manadsgivare/">https://expo.se/stod-expo/bli-poddvan/</a></p> <p dir="ltr">Prenumerera på Expo: <a href= "https://expo.se/tidskriften/prenumerera">https://expo.se/tidskriften/prenumerera</a> </p> <p>---</p> <p>Studio Expo ger dig som lyssnar fördjupningar om våra avslöjanden, mer om våra granskningar och analyser av högextrema tendenser. Varje vecka i din poddspelare!</p> <p>Expo är en religiöst och partipolitiskt obunden stiftelse. Vi har granskat och bevakat extremhögern sedan 1995 – för en levande demokrati där rasistiska idéer och organisationer saknar inflytande.</p>]]></content:encoded>
			<enclosure length="52517158" type="audio/mpeg" url="https://traffic.libsyn.com/secure/cfd1d86e-6f6c-4f03-b723-512a2bdd4b90/134._100_ar_av_svensk_nazism_Del_3_av_4__Raskrigarna.mp3?dest-id=3415931" />
			<itunes:duration>54:43</itunes:duration>
			<itunes:explicit>false</itunes:explicit>
			<itunes:keywords />
			<itunes:subtitle><![CDATA[I år är det 100 år sedan det första svenska nazistpartiet bildades. Trots vetskapen om nazismens fruktansvärda konsekvenser har nya anhängare fortsatt att attraheras av dess rasism och antidemokratiska idéer. 100 år av obruten nazistisk...]]></itunes:subtitle>
			<itunes:episode>134</itunes:episode>
			<itunes:episodeType>full</itunes:episodeType>
			<itunes:author>Stiftelsen Expo</itunes:author>
		</item>

	</channel>
</rss>
"""

data_scocconomics_captivate_podcast_feed_sample = """<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet href="https://feeds.captivate.fm/style.xsl" type="text/xsl"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:atom="http://www.w3.org/2005/Atom" version="2.0"
    xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0"
    xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:podcast="https://podcastindex.org/namespace/1.0">
    <channel>
        <atom:link href="https://feeds.captivate.fm/scocconomics/" rel="self"
            type="application/rss+xml" />
        <title><![CDATA[Scocconomics]]></title>
        <podcast:guid>9d17ee26-3293-52b7-a4c1-30498e8c66f1</podcast:guid>
        <lastBuildDate>Fri, 20 Dec 2024 16:15:23 +0000</lastBuildDate>
        <generator>Captivate.fm</generator>
        <language><![CDATA[sv]]></language>
        <copyright><![CDATA[All rights reserved]]></copyright>
        <managingEditor>Sandro Scocco, Josefin Nauri</managingEditor>
        <itunes:summary><![CDATA[Du vill ha bättre koll när du lyssnar på ekonominyheterna. Du vill ha argument och kött på benen i debatten. Lyssna när Sandro Scocco och Josefin Nauri snackar om pengar & politik i podden Socconomics, som bytt namn från Nya pengar & politik (eller som de säger om Ullevi: Gamla Nya Pengar & politik)]]></itunes:summary>
        <image>
            <url>
                https://artwork.captivate.fm/bcaaf043-4b41-4e77-9581-006ad7a93a71/K8MQdxBfYHuU-aMlxCHIcunW.jpg</url>
            <title>Scocconomics</title>
            <link><![CDATA[https://www.youtube.com/channel/UCgoL-R3Y9bhIbesrGdTaodA]]></link>
        </image>
        <itunes:image
            href="https://artwork.captivate.fm/bcaaf043-4b41-4e77-9581-006ad7a93a71/K8MQdxBfYHuU-aMlxCHIcunW.jpg" />
        <itunes:owner>
            <itunes:name>Sandro Scocco, Josefin Nauri</itunes:name>
        </itunes:owner>
        <itunes:author>Sandro Scocco, Josefin Nauri</itunes:author>
        <description>Du vill ha bättre koll när du lyssnar på ekonominyheterna. Du vill ha argument
            och kött på benen i debatten. Lyssna när Sandro Scocco och Josefin Nauri snackar om
            pengar &amp; politik i podden Socconomics, som bytt namn från Nya pengar &amp; politik
            (eller som de säger om Ullevi: Gamla Nya Pengar &amp; politik)</description>
        <link>https://www.youtube.com/channel/UCgoL-R3Y9bhIbesrGdTaodA</link>
        <atom:link href="https://pubsubhubbub.appspot.com" rel="hub" />
        <itunes:subtitle><![CDATA[Du vill ha bättre koll på ekonomidiskussionerna]]></itunes:subtitle>
        <itunes:explicit>false</itunes:explicit>
        <itunes:type>episodic</itunes:type>
        <itunes:category text="News">
            <itunes:category text="Politics" />
        </itunes:category>
        <itunes:category text="Science">
            <itunes:category text="Social Sciences" />
        </itunes:category>
        <itunes:category text="News">
            <itunes:category text="Business News" />
        </itunes:category>
        <itunes:new-feed-url>https://feeds.captivate.fm/scocconomics/</itunes:new-feed-url>
        <podcast:locked>no</podcast:locked>
        <podcast:medium>podcast</podcast:medium>
        <item>
            <title>Om krisen eller kriget kommer: Lycka till!</title>
            <itunes:title>Om krisen eller kriget kommer: Lycka till!</itunes:title>
            <description><![CDATA[<p>Sandro och Josefin rundar av året med betraktelser över privatiserad krisberedskap, Elisabeth Svantesson i riddarrustning och nästa års förväntade ekonomisk-politiska irrationalitet.</p>]]></description>
            <content:encoded><![CDATA[<p>Sandro och Josefin rundar av året med betraktelser över privatiserad krisberedskap, Elisabeth Svantesson i riddarrustning och nästa års förväntade ekonomisk-politiska irrationalitet.</p>]]></content:encoded>
            <link><![CDATA[https://scocconomics.captivate.fm/episode/om-krisen-eller-kriget-kommer-lycka-till]]></link>
            <guid isPermaLink="false">90b828fc-f46a-4766-8a4e-68a61b1bfafe</guid>
            <itunes:image
                href="https://artwork.captivate.fm/bcaaf043-4b41-4e77-9581-006ad7a93a71/K8MQdxBfYHuU-aMlxCHIcunW.jpg" />
            <dc:creator><![CDATA[Sandro Scocco, Josefin Nauri]]></dc:creator>
            <pubDate>Fri, 20 Dec 2024 17:15:00 +0100</pubDate>
            <enclosure
                url="https://podcasts.captivate.fm/media/43adae10-dec4-416e-9f67-f394f07d68c8/ScocconomicsE58mp3.mp3"
                length="157254452" type="audio/mpeg" />
            <itunes:duration>01:21:54</itunes:duration>
            <itunes:explicit>false</itunes:explicit>
            <itunes:episodeType>full</itunes:episodeType>
            <itunes:season>4</itunes:season>
            <itunes:episode>58</itunes:episode>
            <itunes:season>4</itunes:season>
            <podcast:episode>58</podcast:episode>
            <podcast:season>4</podcast:season>
            <itunes:author>Sandro Scocco, Josefin Nauri</itunes:author>
        </item>
        <item>
            <title>Ida Gabrielsson gästar Scocconomics!</title>
            <itunes:title>Ida Gabrielsson gästar Scocconomics!</itunes:title>
            <description><![CDATA[<p>Scocconomics har både publik och celebert besök denna vecka. Sandro och Josefin resonerar med Vänsterpartiets ekonomisk-politiska talesperson om välfärd, budgetrestriktioner och läckande avloppsrör inför en entusiastisk publik på Socialistiskt forum i Stockholm.</p>]]></description>
            <content:encoded><![CDATA[<p>Scocconomics har både publik och celebert besök denna vecka. Sandro och Josefin resonerar med Vänsterpartiets ekonomisk-politiska talesperson om välfärd, budgetrestriktioner och läckande avloppsrör inför en entusiastisk publik på Socialistiskt forum i Stockholm.</p>]]></content:encoded>
            <link><![CDATA[https://scocconomics.captivate.fm/episode/ida-gabrielsson-gastar-scocconomics]]></link>
            <guid isPermaLink="false">e0edb316-103c-422a-a99d-b1b99da34291</guid>
            <itunes:image
                href="https://artwork.captivate.fm/bcaaf043-4b41-4e77-9581-006ad7a93a71/K8MQdxBfYHuU-aMlxCHIcunW.jpg" />
            <dc:creator><![CDATA[Sandro Scocco, Josefin Nauri]]></dc:creator>
            <pubDate>Tue, 10 Dec 2024 09:35:00 +0100</pubDate>
            <enclosure
                url="https://podcasts.captivate.fm/media/19932a7d-f4c6-4742-8c69-c1377b8fff28/scocconomicsE57.mp3"
                length="114132788" type="audio/mpeg" />
            <itunes:duration>59:26</itunes:duration>
            <itunes:explicit>false</itunes:explicit>
            <itunes:episodeType>full</itunes:episodeType>
            <itunes:season>4</itunes:season>
            <itunes:episode>57</itunes:episode>
            <itunes:season>4</itunes:season>
            <podcast:episode>57</podcast:episode>
            <podcast:season>4</podcast:season>
            <itunes:author>Sandro Scocco, Josefin Nauri</itunes:author>
        </item>
    </channel>
</rss>
"""

data_tyckpressen_transistor_podcast = """<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet href="https://feeds.transistor.fm/stylesheet.xsl" type="text/xsl"?>
<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:podcast="https://podcastindex.org/namespace/1.0">
  <channel>
    <atom:link rel="self" type="application/atom+xml" href="https://feeds.transistor.fm/tyckpressen" title="MP3 Audio"/>
    <atom:link rel="hub" href="https://pubsubhubbub.appspot.com/"/>
    <podcast:podping usesPodping="true"/>
    <title>Tyckpressen</title>
    <generator>Transistor (https://transistor.fm)</generator>
    <itunes:new-feed-url>https://feeds.transistor.fm/tyckpressen</itunes:new-feed-url>
    <description>Tyckpressen är Dagens ETC:s ledarpodd, med tidningens egna ledarskribenter och inbjudna gäster.</description>
    <copyright>© ETC Media AB</copyright>
    <podcast:guid>423a84c0-fc0b-5e2e-8f71-d1d434285b52</podcast:guid>
    <podcast:locked owner="play@etc.se">no</podcast:locked>
    <language>sv</language>
    <pubDate>Fri, 20 Dec 2024 05:00:05 +0100</pubDate>
    <lastBuildDate>Fri, 20 Dec 2024 05:01:09 +0100</lastBuildDate>
    <link>https://play.etc.se/tyckpressen</link>
    <image>
      <url>https://img.transistor.fm/hg5gyOmyY6Wk4X4WR9BgDiHdZjzF9-0oRGS_unPXHno/rs:fill:3000:3000:1/q:60/aHR0cHM6Ly9pbWct/dXBsb2FkLXByb2R1/Y3Rpb24udHJhbnNp/c3Rvci5mbS9zaG93/LzI0MTQ2LzE2MzE3/OTg2NjYtYXJ0d29y/ay5qcGc.jpg</url>
      <title>Tyckpressen</title>
      <link>https://play.etc.se/tyckpressen</link>
    </image>
    <itunes:category text="News"/>
    <itunes:category text="News">
      <itunes:category text="News Commentary"/>
    </itunes:category>
    <itunes:type>episodic</itunes:type>
    <itunes:author>Dagens ETC</itunes:author>
    <itunes:image href="https://img.transistor.fm/hg5gyOmyY6Wk4X4WR9BgDiHdZjzF9-0oRGS_unPXHno/rs:fill:3000:3000:1/q:60/aHR0cHM6Ly9pbWct/dXBsb2FkLXByb2R1/Y3Rpb24udHJhbnNp/c3Rvci5mbS9zaG93/LzI0MTQ2LzE2MzE3/OTg2NjYtYXJ0d29y/ay5qcGc.jpg"/>
    <itunes:summary>Tyckpressen är Dagens ETC:s ledarpodd, med tidningens egna ledarskribenter och inbjudna gäster.</itunes:summary>
    <itunes:subtitle>Tyckpressen är Dagens ETC:s ledarpodd, med tidningens egna ledarskribenter och inbjudna gäster..</itunes:subtitle>
    <itunes:keywords>ETC, Dagens ETC, politik, Play, nyheter</itunes:keywords>
    <itunes:owner>
      <itunes:name>ETC Play</itunes:name>
    </itunes:owner>
    <itunes:complete>No</itunes:complete>
    <itunes:explicit>No</itunes:explicit>
    <item>
      <title>SPEZIAL: Messiah Hallberg – Får man må bra som SVT-anställd? </title>
      <itunes:episode>109</itunes:episode>
      <podcast:episode>109</podcast:episode>
      <itunes:title>SPEZIAL: Messiah Hallberg – Får man må bra som SVT-anställd? </itunes:title>
      <itunes:episodeType>full</itunes:episodeType>
      <guid isPermaLink="false">4104fdbd-5b2e-44fd-af1d-6074eeeff5ca</guid>
      <link>https://share.transistor.fm/s/0c4b1c4c</link>
      <description>
        <![CDATA[<p>Överklasshöger enligt vissa, kommunist enligt andra. Messiah Hallberg har gjort succé som programledare för SVT:s satirprogram Svenska nyheter. Men hur förhåller han sig till public service-ängsligheten? Vad blir reaktionerna när man skämtar om SD, och vem är egentligen Messiahs nemesis? <br> <br> Annie Croona och Max V Karlsson tar tempen på komikern och programledaren som är aktuell med programmet ”Hur fan hamnade vi här?” som har premiär i januari och en stundande färsk säsong av Svenska nyheter. <br> <br> Ansvarig utgivare: Andreas Gustavsson </p>]]>
      </description>
      <content:encoded>
        <![CDATA[<p>Överklasshöger enligt vissa, kommunist enligt andra. Messiah Hallberg har gjort succé som programledare för SVT:s satirprogram Svenska nyheter. Men hur förhåller han sig till public service-ängsligheten? Vad blir reaktionerna när man skämtar om SD, och vem är egentligen Messiahs nemesis? <br> <br> Annie Croona och Max V Karlsson tar tempen på komikern och programledaren som är aktuell med programmet ”Hur fan hamnade vi här?” som har premiär i januari och en stundande färsk säsong av Svenska nyheter. <br> <br> Ansvarig utgivare: Andreas Gustavsson </p>]]>
      </content:encoded>
      <pubDate>Fri, 20 Dec 2024 05:00:00 +0100</pubDate>
      <author>Dagens ETC</author>
      <enclosure url="https://media.transistor.fm/0c4b1c4c/f752282f.mp3" length="69485493" type="audio/mpeg"/>
      <itunes:author>Dagens ETC</itunes:author>
      <itunes:image href="https://img.transistor.fm/MIVjVl8-26ORx23EMziHuQn4x0OCglHeBfVcOJd1tGo/rs:fill:3000:3000:1/q:60/aHR0cHM6Ly9pbWct/dXBsb2FkLXByb2R1/Y3Rpb24udHJhbnNp/c3Rvci5mbS81YmZk/NzBhNjNhYTI4ZjFm/OTdhYmE3MWNjZTE0/MmYyYi5wbmc.jpg"/>
      <itunes:duration>2893</itunes:duration>
      <itunes:summary>
        <![CDATA[<p>Överklasshöger enligt vissa, kommunist enligt andra. Messiah Hallberg har gjort succé som programledare för SVT:s satirprogram Svenska nyheter. Men hur förhåller han sig till public service-ängsligheten? Vad blir reaktionerna när man skämtar om SD, och vem är egentligen Messiahs nemesis? <br> <br> Annie Croona och Max V Karlsson tar tempen på komikern och programledaren som är aktuell med programmet ”Hur fan hamnade vi här?” som har premiär i januari och en stundande färsk säsong av Svenska nyheter. <br> <br> Ansvarig utgivare: Andreas Gustavsson </p>]]>
      </itunes:summary>
      <itunes:keywords>Messiah Hallberg, Annie Croona, Max V Karlsson, Svenska nyheter</itunes:keywords>
      <itunes:explicit>No</itunes:explicit>
    </item>
    <item>
      <title>88. Vilket politiskt våld glorifierar du, lille vän? </title>
      <itunes:episode>108</itunes:episode>
      <podcast:episode>108</podcast:episode>
      <itunes:title>88. Vilket politiskt våld glorifierar du, lille vän? </itunes:title>
      <itunes:episodeType>full</itunes:episodeType>
      <guid isPermaLink="false">581d68d5-bbbb-4904-9cf6-bf5e7bf990f6</guid>
      <link>https://share.transistor.fm/s/4cebf74a</link>
      <description>
        <![CDATA[<p>Ett uppmärksammat vd-mord skrämmer livet ur amerikanska bolagstoppar. Men på world wide web tokhyllas den 26-åriga misstänkta mördaren med snuskiga TikToks och solidaritetsbudskap. Fick mördaren precis som han ville? Finns det andra politiska mord som uppnått sitt uttalade mål? Och hur mår alla, egentligen?<br> <br>Veckans avsnitt handlar om mord och våld i politikens värld. Panel är Annie Croona, Henrik Jalalian och Max V Karlsson. Glad lyssning! <br> <br>Ansvarig utgivare: Andreas Gustavsson</p>]]>
      </description>
      <content:encoded>
        <![CDATA[<p>Ett uppmärksammat vd-mord skrämmer livet ur amerikanska bolagstoppar. Men på world wide web tokhyllas den 26-åriga misstänkta mördaren med snuskiga TikToks och solidaritetsbudskap. Fick mördaren precis som han ville? Finns det andra politiska mord som uppnått sitt uttalade mål? Och hur mår alla, egentligen?<br> <br>Veckans avsnitt handlar om mord och våld i politikens värld. Panel är Annie Croona, Henrik Jalalian och Max V Karlsson. Glad lyssning! <br> <br>Ansvarig utgivare: Andreas Gustavsson</p>]]>
      </content:encoded>
      <pubDate>Thu, 12 Dec 2024 15:50:19 +0100</pubDate>
      <author>Dagens ETC</author>
      <enclosure url="https://media.transistor.fm/4cebf74a/f0dbc56f.mp3" length="29896167" type="audio/mpeg"/>
      <itunes:author>Dagens ETC</itunes:author>
      <itunes:image href="https://img.transistor.fm/emDpWri5lLJNZ770CviYjLFfZ43UPxsc8xzJxCmA93k/rs:fill:3000:3000:1/q:60/aHR0cHM6Ly9pbWct/dXBsb2FkLXByb2R1/Y3Rpb24udHJhbnNp/c3Rvci5mbS9hYzIx/Y2IyOTYxMjRhODIy/MjY4OTNlMjI0MmI1/YzNjOS5wbmc.jpg"/>
      <itunes:duration>1867</itunes:duration>
      <itunes:summary>
        <![CDATA[<p>Ett uppmärksammat vd-mord skrämmer livet ur amerikanska bolagstoppar. Men på world wide web tokhyllas den 26-åriga misstänkta mördaren med snuskiga TikToks och solidaritetsbudskap. Fick mördaren precis som han ville? Finns det andra politiska mord som uppnått sitt uttalade mål? Och hur mår alla, egentligen?<br> <br>Veckans avsnitt handlar om mord och våld i politikens värld. Panel är Annie Croona, Henrik Jalalian och Max V Karlsson. Glad lyssning! <br> <br>Ansvarig utgivare: Andreas Gustavsson</p>]]>
      </itunes:summary>
      <itunes:keywords>ETC, Dagens ETC, politik, Play, nyheter</itunes:keywords>
      <itunes:explicit>No</itunes:explicit>
    </item>
  </channel>
</rss>
"""