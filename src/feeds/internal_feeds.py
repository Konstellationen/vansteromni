from django.contrib.syndication.views import Feed as SyndicationFeed
from django.shortcuts import get_object_or_404
from .models import  FeedCategory, Item
from .models import Feed as FeedModel
from django.http import Http404
from django.core.paginator import Paginator

class CategoryFeed(SyndicationFeed):

    def get_object(self, request, slug, item_type):
        if item_type not in ["curated", "all"]:
            raise Http404
        self.item_type = item_type
        self.page = int(request.GET.get('page', 1))
        
        self.path = request.build_absolute_uri('')
        return get_object_or_404(FeedCategory, slug=slug)

    def title(self, obj):
        translations = {
            'all': 'alla',
            'curated': 'utvalda'
        }
        item_type = translations.get(self.item_type, self.item_type)
        return f"Mediakollen: {obj.name} ({item_type})"

    def link(self, obj):
        return f"/rss/{obj.slug}/{self.item_type}/"
    
    def feed_url(self, obj):
        return f"/rss/{obj.slug}/{self.item_type}/"
    
    def description(self, obj):
        return obj.description or "No description available for this category."

    def items(self, obj):
        feeds_in_category = FeedModel.objects.filter(category=obj)
        
        if self.item_type == "curated":
            entries = Item.objects.filter(feed__in=feeds_in_category, show_item=True).order_by('-id')
        else: 
            entries = Item.objects.filter(feed__in=feeds_in_category).order_by('-id')

        paginator = Paginator(entries, 50);
        return [] if self.page > paginator.count else paginator.get_page(self.page)

    def item_title(self, item):
        return item.title
        
    def item_description(self, item):
        return item.summary
    
    def item_link(self, item):
        return item.link
    
    def item_pubdate(self, item):
        return item.published
    
    def item_guid_is_permalink(self):
        return False
    
    def item_categories(self, item):
        return [item.feed.name]
    
    def item_author_name(self,item):
        return item.feed.id
    
    def item_guid(self, item):
        return f"{self.path}item/{item.id}"
