from django.contrib import admin

from .models import Feed, Item, ItemFeedMap, FeedCategory

admin.site.register(Feed)
admin.site.register(Item)
admin.site.register(ItemFeedMap)
admin.site.register(FeedCategory)
