from django.test import TestCase

import httpretty
from .models import Feed, ItemFeedMap
from .functions import feed_string_to_items, parse_feed, get_feed_data, feed_date_format_to_django_date, feed_normal_date_format_to_django_date
from .tests_data import data_konstellationen_feed_sample, data_arbetaren_feed_sample, data_etc_feed_sample, data_aftonbladet_podcast_feed_sample, data_studio_expo_podcast_libsyn_feed_sample, data_scocconomics_captivate_podcast_feed_sample, data_tyckpressen_transistor_podcast

class HelperFunctionsTests(TestCase):
    def test_feed_date_format_to_django_date(self):
        """
        Test formatting
        """
        new_format_string = feed_date_format_to_django_date("Wed, 19 Jul 2023 07:49:27 +0000")
        self.assertIs(1, 1)

    def test_feed_normal_date_format_to_django_date(self):
        """
        Test formatting with date string from atom feed
        """
        new_format_string = feed_normal_date_format_to_django_date("2023-08-03T14:00:00.000Z")
        self.assertIs(1, 1)

class FunctionsTests(TestCase):
    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_get_feed_data(self):
        httpretty.register_uri(
            httpretty.GET,
            "https://www.arbetaren.se/feed/",
            body=data_arbetaren_feed_sample
        )
        data = get_feed_data("https://www.arbetaren.se/feed/")
        self.assertIs(1, 1)

    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_parse_feed(self):
        httpretty.register_uri(
            httpretty.GET,
            "https://www.arbetaren.se/feed/",
            body=data_arbetaren_feed_sample
        )
        item_feed_map = ItemFeedMap(
            item_feed_map_name="Arbetaren",
            items="channel/item",
            title="title",
            link="link",
            item_id="guid",
            published="pubDate",
            updated="doesnotexist",
            content="content",
            summary="description",
            image_url="media/media",
            audio_url="doesnotexist",
        )
        item_feed_map.save()
        feed = Feed(name="Arbetaren", url="https://www.arbetaren.se/feed/", description="Arbetaren feed", item_feed_map=item_feed_map)
        feed.save()
        parse_feed(feed)
        self.assertIs(1, 1)

    def test_feed_string_to_items_arbetaren_sample(self):
        """
        Test at sample from arbetaren
        """
        item_feed_map = ItemFeedMap(
            items="channel/item",
            title="title",
            link="link",
            item_id="guid",
            published="pubDate",
            updated="doesnotexist",
            content="content",
            summary="description",
            image_url="media/media",
            audio_url="doesnotexist"
        )
        item_feed_map.save()
        items = feed_string_to_items(data_arbetaren_feed_sample, item_feed_map)
        self.assertIs(len(items), 1)
        self.assertEqual("Därför måste vi prata om sexbarnsfamiljernas lyxande", items[0]["title"])
        self.assertEqual("https://www.arbetaren.se/2023/07/19/darfor-maste-vi-prata-om-sexbarnsfamiljernas-lyxande/", items[0]["link"])
        self.assertEqual("Edvin Alpros förklarar från sin segelbåt varför sexbarnsfamiljers lyxliv på existensminimum, män med smink och VAB-missbruket är de viktigaste debatterna för varje entreprenör att driva just nu.", items[0]["summary"])


    def test_feed_string_to_items_etc_sample(self):
        """
        Test at sample from etc
        """
        item_feed_map = ItemFeedMap(
            items="channel/item",
            title="title",
            link="link",
            item_id="guid",
            published="pubDate",
            updated="doesnotexist",
            content="content",
            summary="description",
            image_url="enclosure",
            audio_url="doesnotexist"
        )
        item_feed_map.save()
        items = feed_string_to_items(data_etc_feed_sample, item_feed_map)
        self.assertIs(len(items), 2)
        self.assertEqual("Moderat toppnamn döms till fängelse efter flera våldsbrott", items[0]["title"])
        self.assertEqual("https://www.etc.se/inrikes/moderat-toppnamn-doems-till-faengelse-efter-flera-vaaldsbrott?utm_source=rss", items[0]["link"])
        self.assertEqual("https://cdn.publisher-live.etc.nu/swp/uc3g8l/media/2023072412078_1d2f11930cfcbf3b2857d6d935bccab395a701b3cbec81ccc96f0e29ac72ccb0.png", items[0]["image_url"])


    def test_feed_string_to_items_aftonbladet_podcast_sample(self):
        """
        Test parsing sample from Aftonbladet podcast feed
        """
        # Unfortunately, Aftonbladet podcast has no field for external url (link)
        item_feed_map = ItemFeedMap(
            items="channel/item",
            title="title",
            link="enclosure",
            item_id="guid",
            published="pubDate",
            updated="pubDate",
            content="description",
            summary="description",
            image_url="image/url",
            audio_url="enclosure"
        )
        item_feed_map.save()
        items = feed_string_to_items(data_aftonbladet_podcast_feed_sample, item_feed_map)
    
        self.assertIs(len(items), 4)
        self.assertEqual("Festa med Henrik Brandão Jönsson och Victor Malms twitterfeed", items[0]["title"])

        # Test first item attributes
        first_item = items[0]
        self.assertEqual("https://flex2.acast.com/s/cafe-bambino-podden/u/dd-ab.akamaized.net/ab/vod/2024/11/674845313234850008db53a2/podcast_128.mp3", first_item["link"])
        self.assertEqual("Kära bambinos, i årets sista avsnitt går vi igenom lite saker vi inte fått sagt under hösten OCH vi har en julutlottning som heter duga. Tone sympatiserar med Henrik Brandão Jönsson, är trött på amerikanska poptjejer och vill inte dö hos gynekologen. Karin räknar tweets och undrar vad fan som menas med vänsterns Joe Rogan. Tack för en otrolig höst alla älskade bambinos, vi ses i slutet av januari! Följ oss på Instagram @cafebambino4ever för mer information om utlottning och livepodd! Den 30 januari kommer Café Bambino till Brogatan i Malmö och livepoddar. Nu kan du köpa din biljett här: https://billetto.se/e/livepod-cafe-bambino-biljetter-1123372?utm_source=organiser=share=copy_link=2", first_item["content"])
        self.assertEqual("https://images.stream.schibsted.media/users/ab_/images/73a860b0f2db7cd684e0165b8baa9a18.jpg?t%5B%5D=3000x3000q80", first_item["image_url"])
        self.assertEqual("https://flex2.acast.com/s/cafe-bambino-podden/u/dd-ab.akamaized.net/ab/vod/2024/11/674845313234850008db53a2/podcast_128.mp3", first_item["audio_url"])

        # Test second item attributes
        second_item = items[1]
        self.assertEqual("https://flex2.acast.com/s/cafe-bambino-podden/u/dd-ab.akamaized.net/ab/vod/2024/11/673f6b7c3234850008db453d/podcast_128.mp3", second_item["link"])
        self.assertEqual("Vänsterpopulism, tyska konservativa kommunister och vad högern gör rätt", second_item["title"])
        self.assertEqual("I veckans avsnitt funderar vi på vänsterpopulism, ett par veckor efter att högerpopulismen vann valet i USA. Karin har fått dille på en ny, konstig politiker i Tyskland. Sahra Wagenknecht är kvinnan som på kort tid byggt ett anti-elitistiskt parti, döpt efter sig själv. Kärnväljarna består främst av arbetare, lågutbildade personer och personer med invandrarbakgrund, grupper som vänstern ständigt gnäller över att ha förlorat. Wagenknecht kommer från ett traditionellt vänsterhåll, men många av hennes värderingar är konservativa. Hon hatar cykelbanor och förespråkar en stram migrationspolitik, samtidigt som hon riktar stark kritik mot Israel och vill ha lägre energipriser. Flera framgångsrika politiker ritar numera sina egna politiska kartor utifrån väljarnas triggerpunkter och framförallt högern har lyckats identifiera dessa. Medan de bygger populistiska, politiska allianser haltar amerikanska liberaler och europeiska socialdemokrater långsamt efter. Varför då? När våras det egentligen för vänsterpopulismen? Är tyska Sahra Wagenknecht ett exempel på den? Vilka populistiska förslag går Tone till val på? Och så blir det tjafs om Richard Jomshof.", second_item["content"])
        self.assertEqual("https://images.stream.schibsted.media/users/ab_/images/73a860b0f2db7cd684e0165b8baa9a18.jpg?t%5B%5D=3000x3000q80", second_item["image_url"])
        self.assertEqual("https://flex2.acast.com/s/cafe-bambino-podden/u/dd-ab.akamaized.net/ab/vod/2024/11/673f6b7c3234850008db453d/podcast_128.mp3", second_item["audio_url"])

    def test_feed_string_to_items_data_studio_expo_podcast_libsyn_feed_sample(self):
        """
        Test parsing sample from Studio Expo podcast feed. Using Libsyn
        """
        item_feed_map = ItemFeedMap(
            items="channel/item",
            title="title",
            link="link",
            item_id="guid",
            published="pubDate",
            updated="pubDate",
            content="description",
            summary="description",
            image_url="{http://www.itunes.com/dtds/podcast-1.0.dtd}image",
            audio_url="enclosure"
        )
        item_feed_map.save()
        items = feed_string_to_items(data_studio_expo_podcast_libsyn_feed_sample, item_feed_map)

        self.assertEqual(len(items), 2)

        # Test first item attributes
        first_item = items[0]
        self.assertEqual("https://sites.libsyn.com/412148/135-100-r-av-svensk-nazism-del-4-av-4-gatuaktivisterna", first_item["link"])
        self.assertEqual('<p>I år är det 100 år sedan det första svenska nazistpartiet bildades. Trots vetskapen om nazismens fruktansvärda konsekvenser har nya anhängare fortsatt att attraheras av dess rasism och antidemokratiska idéer. 100 år av obruten nazistisk organisering. Det är en brokig historia fylld av våld, terror och gatuaktivism. Men även valsatsningar och entreprenörskap. Mängder av partier och organisationer har bildats och försvunnit. Det har varit otaliga interna strider och fraktionsbildningar, men även återkommande enhetsförsök. Expo kommer i fyra avsnitt att berätta om den här heterogena rörelsens utveckling och olika faser från de första partibildningarna till dagens slagsmålsklubbar.  </p> <p><mark class="has-inline-color has-black-color">Vi har nu kommit fram till</mark> 2000-talet. Ett mord i Salem som får stora delar av den svenska nazistmiljön att enas och under det nya millenniets första decennium genomförs flera samlande demonstrationer innan konflikter och interna strider återigen sätter stopp för sammanhållningen. </p> <p>Det fjärde och avslutande avsnittet i serien om 100 år av svensk nazism handlar om vår samtida nazistiska historia — där det parallellt med partisatsningar går att se hur lösare nätverk och lokala aktivistgrupper slår igenom. Det är en trendkänslig, internationellt uppkopplad och snabbt skiftande miljö som jämte nazismen ger plats för identitärer, nyfascister, fria nationalister, realister, alt-right-aktivister, medborgarjournalister och aktivklubbar. Och återigen är våldet den nazistiska idévärldens följeslagare — med bombattentat, mord och skoldåd. </p> <p><mark class="has-inline-color has-black-color">Hur kan vi förstå det myller</mark> av organisationsformer vi ser växa fram under 2000-talet? Hur utnyttjar extremhögern den digitala revolutionen? Och hur påverkar miljöns nu nästan 100 åriga historia den samtida nazismen? Vi är framme vid smörgåsbordsnazismen och de så kallade ensamagerande terroristernas epok.</p> <p>Gäster i studion är Daniel Poohl, vd på Expo och Anders Dalsbro, reporter på Expo.</p> <p>Programledare: Anna Fröjd</p> <p dir="ltr">---</p> <p dir="ltr"><strong>Läs mer:</strong></p> <p>Studio Expo: 100 år av svensk nazism – Del 1 av 4: Pionjärerna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-pionjarerna/</a> </p> <p>Studio Expo: 100 år av svensk nazism – Del 2 av 4: Övervintrarna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-overvintrarna/</a></p> <p>Studio Expo: 100 år av svensk nazism – del 3 av 4: Raskrigarna <a href= "https://expo.se/podcasts/100-ar-av-svensk-nazism-raskrigarna/">https://expo.se/podcasts/100-ar-av-svensk-nazism-raskrigarna/</a> </p> <p>#4-2023 Tidskriften Expo, 100 år av svensk nazism: <a href= "https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/">https://expo.se/lar-dig-mer/tidskriften/hundra-ar-av-svensk-nazism/</a> </p> <p>Expo wiki – Nationalsocialistisk front (NSF): <a href= "https://expo.se/lar-dig-mer/wiki/nationalsocialistisk-front-nsf/">https://expo.se/lar-dig-mer/wiki/nationalsocialistisk-front-nsf/</a> </p> <p>Expo wiki – Nordiska förbundet: <a href= "https://expo.se/lar-dig-mer/wiki/nordiska-forbundet/">https://expo.se/lar-dig-mer/wiki/nordiska-forbundet/</a> </p> <p>Expo wiki – Nationaldemokraterna: <a href= "https://expo.se/lar-dig-mer/wiki/nationaldemokraterna/">https://expo.se/lar-dig-mer/wiki/nationaldemokraterna/</a> </p> <p>Expo wiki – Nordiska motståndsrörelsen (NMR): <a href= "https://expo.se/lar-dig-mer/wiki/nordiska-motstandsrorelsen-nmr/">https://expo.se/lar-dig-mer/wiki/nordiska-motstandsrorelsen-nmr/</a> </p> <p>Expo wiki – Alt-right: <a href= "https://expo.se/lar-dig-mer/wiki/alt-right-rorelsen/">https://expo.se/lar-dig-mer/wiki/alt-right-rorelsen/</a> </p> <p> </p> <p dir="ltr"><strong><span id= "docs-internal-guid-e46c9aa8-7fff-473e-365f-a511c6ad2020">Expo behöver ditt stöd</span></strong></p> <p dir="ltr">Bli poddvän här: <a href= "https://expo.se/stod-expo/manadsgivare/">https://expo.se/stod-expo/bli-poddvan/</a></p> <p dir="ltr">Prenumerera på Expo: <a href= "https://expo.se/tidskriften/prenumerera">https://expo.se/tidskriften/prenumerera</a> </p> <p>---</p> <p><strong>Studio Expo ger dig</strong> som lyssnar fördjupningar om våra avslöjanden, mer om våra granskningar och analyser av högextrema tendenser. Varje vecka i din poddspelare!</p> <p>Expo är en religiöst och partipolitiskt obunden stiftelse. Vi har granskat och bevakat extremhögern sedan 1995 – för en levande demokrati där rasistiska idéer och organisationer saknar inflytande.</p>', first_item["content"])

        self.assertEqual("https://static.libsyn.com/p/assets/2/3/f/1/23f122d583a7724fd959afa2a1bf1c87/Kopia_av_MALL_Studio_Expo_Cover_Sommar_Spotify_5.jpg", first_item["image_url"])
        self.assertEqual("https://traffic.libsyn.com/secure/cfd1d86e-6f6c-4f03-b723-512a2bdd4b90/135._100_ar_av_svensk_nazism_del_4_av_4__Gatuaktivisterna.mp3?dest-id=3415931", first_item["audio_url"])

    def test_feed_string_to_items_data_scocconomics_captivate_podcast_feed_sample(self):
        """
        Test parsing sample from Scocconomics podcast feed. Using Captivate.
        """
        # Unfortunately, Aftonbladet podcast has no field for external url (link)
        item_feed_map = ItemFeedMap(
            items="channel/item",
            title="title",
            link="link",
            item_id="guid",
            published="pubDate",
            updated="pubDate",
            content="description",
            summary="description",
            image_url="{http://www.itunes.com/dtds/podcast-1.0.dtd}image",
            audio_url="enclosure"
        )
        item_feed_map.save()
        items = feed_string_to_items(data_scocconomics_captivate_podcast_feed_sample, item_feed_map)

        self.assertEqual(len(items), 2)

        # Test first item attributes
        first_item = items[0]
        self.assertEqual("https://scocconomics.captivate.fm/episode/om-krisen-eller-kriget-kommer-lycka-till", first_item["link"])
        self.assertEqual("Om krisen eller kriget kommer: Lycka till!", first_item["title"])
        self.assertEqual("<p>Sandro och Josefin rundar av året med betraktelser över privatiserad krisberedskap, Elisabeth Svantesson i riddarrustning och nästa års förväntade ekonomisk-politiska irrationalitet.</p>", first_item["summary"])
        self.assertEqual("https://artwork.captivate.fm/bcaaf043-4b41-4e77-9581-006ad7a93a71/K8MQdxBfYHuU-aMlxCHIcunW.jpg", first_item["image_url"])
        self.assertEqual("https://podcasts.captivate.fm/media/43adae10-dec4-416e-9f67-f394f07d68c8/ScocconomicsE58mp3.mp3", first_item["audio_url"])

    def test_feed_string_to_items_data_tyckpressen_transistor_podcast(self):
        """
        Test parsing sample from Tyckpressen podcast feed, using Transistor
        """
        # Unfortunately, Aftonbladet podcast has no field for external url (link)
        item_feed_map = ItemFeedMap(
            items="channel/item",
            title="title",
            link="link",
            item_id="guid",
            published="pubDate",
            updated="pubDate",
            content="description",
            summary="description",
            image_url="{http://www.itunes.com/dtds/podcast-1.0.dtd}image",
            audio_url="enclosure"
        )
        item_feed_map.save()
        items = feed_string_to_items(data_tyckpressen_transistor_podcast, item_feed_map)

        self.assertEqual(len(items), 2)

        # Test first item attributes
        first_item = items[0]
        self.assertEqual("https://share.transistor.fm/s/0c4b1c4c", first_item["link"])
        self.assertEqual("SPEZIAL: Messiah Hallberg – Får man må bra som SVT-anställd?", first_item["title"])
        self.assertEqual("<p>Överklasshöger enligt vissa, kommunist enligt andra. Messiah Hallberg har gjort succé som programledare för SVT:s satirprogram Svenska nyheter. Men hur förhåller han sig till public service-ängsligheten? Vad blir reaktionerna när man skämtar om SD, och vem är egentligen Messiahs nemesis? <br> <br> Annie Croona och Max V Karlsson tar tempen på komikern och programledaren som är aktuell med programmet ”Hur fan hamnade vi här?” som har premiär i januari och en stundande färsk säsong av Svenska nyheter. <br> <br> Ansvarig utgivare: Andreas Gustavsson </p>", first_item["summary"])
        self.assertEqual("https://img.transistor.fm/MIVjVl8-26ORx23EMziHuQn4x0OCglHeBfVcOJd1tGo/rs:fill:3000:3000:1/q:60/aHR0cHM6Ly9pbWct/dXBsb2FkLXByb2R1/Y3Rpb24udHJhbnNp/c3Rvci5mbS81YmZk/NzBhNjNhYTI4ZjFm/OTdhYmE3MWNjZTE0/MmYyYi5wbmc.jpg", first_item["image_url"])
        self.assertEqual("https://media.transistor.fm/0c4b1c4c/f752282f.mp3", first_item["audio_url"])
