from django.urls import path
from .internal_feeds import CategoryFeed

from . import views

app_name = "feeds"

urlpatterns = [
    path("", views.index, name="index"),
    path("about", views.about, name="about"),
    path("contact", views.contact, name="contact"),
    path("curated", views.curated, name="curated"),
    path("all", views.all, name="all"),
    path("<int:feed_item_id>/", views.detail, name="detail"),
    path("feed", views.list_feeds, name="list_feeds"),
    path("feed/<int:feed_id>/all", views.feed_all, name="feed_all"),
    path("feed/<int:feed_id>/curated", views.feed_curated, name="feed_curated"),
    path("feed/category/<int:category_id>/all", views.feed_category_all, name="feed_category_all"),
    path("feed/category/<int:category_id>/curated", views.feed_category_curated, name="feed_category_curated"),
    path("api/like/<int:feed_item_id>/", views.api_like, name="api_like"),
    path("api/unlike/<int:feed_item_id>/", views.api_unlike, name="api_unlike"),
    path('rss/<slug:slug>/<str:item_type>/', CategoryFeed(), name='category_rss_feed'),
]
