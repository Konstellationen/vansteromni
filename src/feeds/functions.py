#import xml.etree.ElementTree as ET
from lxml import etree as ET
import requests
import email.utils
import datetime
import time
from django.conf import settings
from django.utils import timezone

from .models import Item

"""
First we have to convert the weird rss format RFC 822 (Wed, 19 Jul 2023 07:49:27 +0000) to
YYYY-MM-DD HH:MM
"""
def feed_date_format_to_django_date(feed_date_str):
    try:
        if not feed_date_str:
            return
   
        date_parsed = email.utils.parsedate(feed_date_str)
        t = time.mktime(date_parsed)
        dt = datetime.datetime.fromtimestamp(t)
        aware_datetime = timezone.make_aware(dt)

        return aware_datetime
    except Exception as e:
        return None

"""
Convert a date string that starts with YYYY-MM-DDTHH:MM
"""
def feed_normal_date_format_to_django_date(feed_date_str):
    try:
        if not feed_date_str:
            return

        isoformat_str = feed_date_str[0:16]
        dt = datetime.datetime.fromisoformat(isoformat_str)
        aware_datetime = timezone.make_aware(dt)

        return aware_datetime
    except Exception as e:
        return None

# Separate function so we easily can mock it
def get_now():
    n = timezone.now()
    return n

def parse_feed(feed):
    print("Parsing feed {}".format(feed.name))

    print("Fetching data from {}".format(feed.url))
    data = get_feed_data(feed.url)
    if not data:
        print("Could not fetch data from {}".format(feed.url))
        return  

    items = feed_string_to_items(data, feed.item_feed_map)
    parsed = len(items)
    print("Got {} items from feed".format(parsed))
    new_items = 0
    errors = 0
    for item in items:
        try:
            existing_items = Item.objects.filter(item_id=item["item_id"])
            if existing_items.count() > 0:
                print("Item already exist with item_id {}".format(item["item_id"]))
                continue

            # The date format for atom feed is different than for RSS
            published = feed_date_format_to_django_date(item["published"])
            if published is None:
                published = feed_normal_date_format_to_django_date(item["published"])

            updated = feed_date_format_to_django_date(item["updated"])
            if updated is None:
                updated = feed_normal_date_format_to_django_date(item["updated"])

            new_item = Item(
                feed=feed,
                title=item["title"],
                link=item["link"],
                item_id=item["item_id"],
                imported=get_now(),
                published=published,
                updated=updated,
                content=item["content"] if item["content"] else "",
                summary=item["summary"],
                image_url=item["image_url"] if item["image_url"] else "",
                audio_url=item["audio_url"] if item["audio_url"] else ""
            )
            new_item.save()
            print("New item: {}".format(new_item))
            new_items += 1
        except Exception as e:
            print("Error occured with item:")
            print(item)
            print(e)
            errors += 1

    print("Parsed {} items. New items: {}. Errors: {}".format(parsed, new_items, errors))

def get_feed_data(feed_url):
    try:
        headers = {'user-agent': 'mediakollen'}
        r = requests.get(feed_url, headers=headers)
        if r.status_code != 200:
            raise Exception("Got status code {} when fetching {}".format(r.status_code, feed_url))

        return r.text
    except Exception as e:
        print("Error reading url {}. Error:".format(feed_url))
        print(e)
        return None

def recover_feed_string(feed_string):
    """Attempt to recover a feed string with namespace issues by removing namespace prefixes"""
    # Handle common XML namespaces that might cause issues
    namespaces = ['itunes', 'media', 'dc', 'content', 'atom']
    
    for ns in namespaces:
        # Remove namespace declarations
        feed_string = feed_string.replace(f'xmlns:{ns}="', 'xmlns_removed="')
        
        # Remove from opening tags
        feed_string = feed_string.replace(f'<{ns}:', '<')
        
        # Remove from closing tags
        feed_string = feed_string.replace(f'</{ns}:', '</')
        
        # Remove from attributes (with space before to avoid matching in URLs)
        feed_string = feed_string.replace(f' {ns}:', ' ')
        
        # Remove from attributes in quotes
        feed_string = feed_string.replace(f'="{ns}:', '="')
    
    return feed_string

def strip_string(s):
    return s.strip() if isinstance(s, str) else s

def feed_string_to_items(feed_string, item_feed_map):
    parser1 = ET.XMLParser(encoding='utf-8', recover=True)
    try:
        # Silly perhaps, but we need to pass bytes to the parser instead of an already encoded string
        root = ET.fromstring(feed_string.encode(), parser=parser1)
    except ET.ParseError:
        # Try again with namespace prefixes removed
        feed_string = recover_feed_string(feed_string)
        root = ET.fromstring(feed_string.encode(), parser=parser1)
        # root = ET.fromstring(feed_string)
    
    l = []
    items = root.findall(item_feed_map.items)
    
    # Get channel-level image URL once (since it's the same for all items)
    channel_image_url = ''
    channel = root.find('channel')
    if channel is not None:
        image = channel.find('image')
        if image is not None:
            url = image.find('url')
            if url is not None and url.text:
                channel_image_url = url.text.strip()
    
    for entry in items:
        try:
            title = strip_string(entry.findtext(item_feed_map.title))
            link = strip_string(entry.findtext(item_feed_map.link))
            # Handle both atom feeds (href) and podcast feeds (url)
            if not link:
                link_elem = entry.find(item_feed_map.link)
                if link_elem is not None:
                    # Try url first (for podcasts)
                    link = link_elem.attrib.get('url', '')
                    if not link:
                        # Try href (for atom feeds)
                        link = strip_string(link_elem.attrib.get('href', ''))

            audio_url = strip_string(entry.findtext(item_feed_map.audio_url))
            if not audio_url:
                audio_url_elem = entry.find(item_feed_map.audio_url)
                if audio_url_elem is not None:
                    # Try url first (for podcasts)
                    audio_url = audio_url_elem.attrib.get('url', '')
                    if not audio_url:
                        # Try href (for atom feeds)
                        audio_url = strip_string(audio_url_elem.attrib.get('href', ''))

            item_id = strip_string(entry.findtext(item_feed_map.item_id))
            published = strip_string(entry.findtext(item_feed_map.published))
            updated = strip_string(entry.findtext(item_feed_map.updated))
            content = strip_string(entry.findtext(item_feed_map.content))
            summary = strip_string(entry.findtext(item_feed_map.summary))
            
            # Handle image_url in both formats:
            # - Some feeds use <enclosure url="..."> or <itunes:image href="...">
            # - Others use <image><url>...</url></image>
            image_elem = entry.find(item_feed_map.image_url)
            if image_elem is not None:
                image_url_item = image_elem.attrib.get('href', '')
                if not image_url_item:
                    image_url_item = image_elem.attrib.get('url', '')
                    if not image_url_item:
                        # If no url attribute, try to get as text
                        image_url_item = strip_string(image_elem.text if image_elem.text else '')
            else:
                image_url_item = ''
            
            item = {
                "title": title,
                "link": link,
                "item_id": item_id,
                "published": published,
                "updated": updated,
                "content": content,
                "summary": summary,
                "image_url": image_url_item or channel_image_url,
                "audio_url": audio_url,
            }
            l.append(item)
        except Exception as e:
            print("Error parsing")
            print(e)

    return l