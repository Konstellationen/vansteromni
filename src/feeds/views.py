from django.urls import reverse
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.core.paginator import Paginator
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.db.models import Prefetch
from django.conf import settings

from datetime import timedelta

from .models import Item
from .models import Feed
from .models import FeedCategory


def index(request):
    now = timezone.now()

    # Identify the FeedCategory instances for news, blog, and podcast
    news_category_name = settings.NEWS_CATEGORY_NAME
    blog_category_name = settings.BLOG_CATEGORY_NAME
    podcast_category_name = settings.PODCAST_CATEGORY_NAME
    event_category_name = settings.EVENT_CATEGORY_NAME

    latest_news_category_length = settings.LATEST_NEWS_CATEGORY_LENGTH
    latest_blog_category_length = settings.LATEST_BLOG_CATEGORY_LENGTH
    latest_podcast_category_length = settings.LATEST_PODCAST_CATEGORY_LENGTH
    latest_event_category_length = settings.LATEST_EVENT_CATEGORY_LENGTH

    news_category = FeedCategory.objects.get(name=news_category_name)
    blog_category = FeedCategory.objects.get(name=blog_category_name)
    podcast_category = FeedCategory.objects.get(name=podcast_category_name)
    event_category = FeedCategory.objects.get(name=event_category_name)

    # Fetch news articles
    news_articles = Item.objects.filter(show_item=True, feed__category=news_category).order_by("-published")[:latest_news_category_length]

    # Fetch blog posts
    blog_posts = Item.objects.filter(show_item=True, feed__category=blog_category).order_by("-published")[:latest_blog_category_length]

    # Fetch podcasts
    podcasts = Item.objects.filter(show_item=True, feed__category=podcast_category).order_by("-published")[:latest_podcast_category_length]

    # Fetch events
    events = Item.objects.filter(show_item=True, feed__category=event_category, published__gte=now).order_by("published")[:latest_event_category_length]

    context = {
        "chosen_news": news_articles,
        "chosen_blog_posts": blog_posts,
        "chosen_podcasts": podcasts,
        "chosen_events": events,
        "news_category_id": news_category.pk,
        "blog_category_id": blog_category.pk,
        "podcast_category_id": podcast_category.pk,
        "event_category_id": event_category.pk
    }

    return render(request, "index.html", context)

def about(request):
    return render(request, "about.html", {})

def contact(request):
    return render(request, "contact.html", {})

def all(request):
    item_list = Item.objects.all().order_by("-published")
    # 50 per page
    paginator = Paginator(item_list, 50)

    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)
    return render(request, "feed/index.html", {
        "feed_type": "all",
        "page_obj": page_obj,
    })

def curated(request):
    item_list = Item.objects.filter(show_item=True).order_by("-published")
    # 50 per page
    paginator = Paginator(item_list, 50)

    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)
    return render(request, "feed/index.html", {
        "feed_type": "curated",
        "page_obj": page_obj,
    })

def feed_all(request, feed_id):
    # Attempt to get the feed for the ID and 404 if we can't find it.
    feed = get_object_or_404(Feed, pk=feed_id)

    # Retrieve feed items for a specific feed, ordered by the published date with the most recent ones first.
    items = Item.objects.filter(feed_id=feed_id).order_by("-published")

    # Paginate the feed's items with 50 items per page.
    paginator = Paginator(items, 50)

    # Use the URL query parameter "page" to select which page to show.
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    return render(request, "feed/index.html", {
        "feed_type": "all",
        "feed": feed,
        "page_obj": page_obj
    })

def feed_curated(request, feed_id):
    # Attempt to get the feed for the ID and 404 if we can't find it.
    feed = get_object_or_404(Feed, pk=feed_id)

    # Retrieve feed items for a specific feed, ordered by the published date with the most recent ones first.
    # Also filter the items based on "show_item" which determines if it's curated or not.
    items = Item.objects.filter(feed_id=feed_id, show_item=True).order_by("-published")

    # Paginate the feed's items with 50 items per page.
    paginator = Paginator(items, 50)

    # Use the URL query parameter "page" to select which page to show.
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    return render(request, "feed/index.html", {
        "feed_type": "curated",
        "feed": feed,
        "page_obj": page_obj
    })

def feed_category_all(request, category_id):
    # Attempt to get the category for the ID and 404 if we can't find it.
    category = get_object_or_404(FeedCategory, pk=category_id)
    all_feed_url = category.all_feed_url

    # Retrieve all items for a specific category ordered by the published date with the most recent ones first.
    items = Item.objects.filter(feed__category=category).order_by("-published")

    # Paginate the feed's items with 50 items per page.
    paginator = Paginator(items, 50)

    # Use the URL query parameter "page" to select which page to show.
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    # return render(request, "cateory/index.html", {
    return render(request, "feed/index.html", {
        "feed_type": "all",
        "category": category,
        "page_obj": page_obj,
        "all_feed_url": all_feed_url
    })

def feed_category_curated(request, category_id):
    # Attempt to get the category for the ID and 404 if we can't find it.
    category = get_object_or_404(FeedCategory, pk=category_id)
    curated_feed_url = category.curated_feed_url

    # Retrieve all items for a specific category ordered by the published date with the most recent ones first.
    items = Item.objects.filter(feed__category=category, show_item=True).order_by("-published")

    # Paginate the feed's items with 50 items per page.
    paginator = Paginator(items, 50)

    # Use the URL query parameter "page" to select which page to show.
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    # return render(request, "category/index.html", {
    return render(request, "feed/index.html", {
        "feed_type": "curated",
        "category": category,
        "page_obj": page_obj,
        "curated_feed_url": curated_feed_url
    })

def detail(request, feed_item_id):
    item = get_object_or_404(Item, pk=feed_item_id)
    return render(request, "feed/item.html", {"item": item})

def list_feeds(request):
    # Order by the name of the category
    categories = FeedCategory.objects.prefetch_related(
        Prefetch(
            'feed_set', 
            queryset=Feed.objects.all().order_by('name')
        )
    ).order_by('name')
    return render(request, "feeds/index.html", {"categories": categories})


@csrf_exempt
def api_like(request, feed_item_id):
    if request.method != "POST":
        return JsonResponse({'success': False,'message':'Method not allowed'}, status=405)

    if not request.user.is_authenticated:
        return JsonResponse({'success': False,'message':'Not logged in'}, status=401)

    try:
        item = Item.objects.get(pk=feed_item_id)
        print("item")
        print(item)
        item.show_item = True
        item.date_processed = timezone.now()
        item.save()

        return JsonResponse({'success': True,'message':'Like successful'})

    except Item.DoesNotExist as e:
        return JsonResponse({'success': False,'message':'Item does not exist'}, status=404)

    except Exception as e:
        print("e")
        print(e)
        return JsonResponse({'success': False,'message':'Unknown error occured when liking'}, status=500)


@csrf_exempt
def api_unlike(request, feed_item_id):
    if request.method != "POST":
        return JsonResponse({'success': False,'message':'Method not allowed'}, status=405)

    if not request.user.is_authenticated:
        return JsonResponse({'success': False,'message':'Not logged in'}, status=401)

    try:
        item = Item.objects.get(pk=feed_item_id)
        print("item")
        print(item)
        item.show_item = False
        item.date_processed = timezone.now()
        item.save()

        return JsonResponse({'success': True,'message':'Unlike successful'})

    except Item.DoesNotExist as e:
        return JsonResponse({'success': False,'message':'Item does not exist'}, status=404)

    except Exception as e:
        print("e")
        print(e)
        return JsonResponse({'success': False,'message':'Unknown error occured when unliking'}, status=500)


