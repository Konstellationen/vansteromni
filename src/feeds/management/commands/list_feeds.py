from django.core.management.base import BaseCommand, CommandError
from feeds.models import Feed


class Command(BaseCommand):
    help = "List feeds"

    def handle(self, *args, **options):
        feeds = Feed.objects.all()
        if feeds.count() > 0:
            for feed in feeds:
                self.stdout.write(
                    self.style.SUCCESS("{} {}".format(feed.pk, feed.name))
                )
        else:
            self.stdout.write(
                self.style.SUCCESS("No feeds in database")
            )