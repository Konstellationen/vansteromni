#!/bin/bash

SRC="src/db.sqlite3"
BACKUP_DIR="../backups/mediakollen/manual"

NOW=$(date +'%Y-%m-%d--%H-%M')

cp "$SRC" "$BACKUP_DIR/db-$NOW.sqlite3"

echo "Backup klar: $BACKUP_DIR/db-$NOW.sqlite3"
